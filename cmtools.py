# -*- coding: utf-8 -*-

"""
Tools for the maximum-entropy solution of the configuration model.
Based on [Squartini et al.14] and the associated code MAX&SAM, and on 
the code corresponding to [Krantz et al.18].

UBCM - Undirected Binary Configuration Model
UWCM - Undirected Weighted Configuration Model
UDCGM - Undirected degree-corrected gravity model

REFS:
-----
[Squartini et al. 14] "Unbiased sampling of network ensembles" arXiv:1406.1197
[Krantz et al. 18]  Ruben Krantz, Valerio Gemmetto, Diego Garlaschelli
				   "Maximum-Entropy Tools for Economic Fitness and Complexity"
				   Entropy 2018, 20, 743;  doi:10.3390/e20100743
[SG11] Squartini, Garlaschelli "Analytical maximum-likelihood method to detect patterns in real networks" arxiv:1103.0701				   
[Mastandrea et al. 14] N.J.Phys 16 (2014) 043022
[Squartini et al. 16] UDCGM arxiv:1610.05494
[Cimini et al. 15] DDCGM  doi: 10.1038/srep15758
[Almog et al. 15a] "Enhanced Gravity Model of trade:reconciling macroeconomic and network models", arxiv:1506.00348
[Bianconi et al.09] Bianconi, Pin Marsili, "Assessing the relevance of node features for network structure"
    PNAS 106, 11433 (2009).http://www.pnas.org/content/early/2009/06/30/0811511106.full.pdf
[Bianconi et al.13] Bianconi, Halu 
[Almog et al. 15b] arxiv:1512.02454

https://docs.scipy.org/doc/scipy/reference/tutorial/optimize.html
"""

import numpy as np
from scipy.optimize import root,minimize,Bounds
from scipy.stats import geom
import cm_ml.cm as cm_cy  # for solve_ml_...
import cm_ml.ficm_cython as ficm_cy  # solve_DCGM 
import networkx as nx
import igraph as ig
import numpy.ma as ma
import warnings
import mpmath # for polylog

from network_tools.measure import snn

# -------------------------------------------
# HELPER FUNCTIONS 

def is_matrix(mat):
    """
    """
    if not isinstance(mat, np.ndarray): return 0
    if not mat.ndim==2:  return 0
    return 1
    
def is_symmetric(mat):
    """ """
    return np.isclose(np.sum(mat-mat.transpose()),0)

def get_undirected_weighted_ig(g):
	""" get undirected weighted igraph from directed """
	if not g.is_directed(): raise ValueError('g is not directed')
	mat = g.get_adjacency(attribute='weight').data
	g_ = ig.Graph.Weighted_Adjacency(mat, mode=ig.ADJ_UNDIRECTED,
				attr="weight", loops=False)
	#copy names				
	g_.vs['name']=	g.vs['name']
	return g_			

def k_test(arg=None):
    """get a degree sample """
    if arg is None:
        return np.array([5,4,3,2,1,1,1,1,1,1,0,0,0,0,0],np.int32)
    elif arg=='powerlaw':
        return np.array(nx.random_powerlaw_tree_sequence(100, tries=5000),dtype=np.int32)
    else: raise ValueError('unrecognized arg')    
    
def s_test(arg=None):
    """get a strength sample """
    if arg is None:
        return 10*np.array([5,4,3,2,1,1,1,1,1,1,0,0,0,0,0])

	
def compare_samplers_UBCM():
    """
    compare random samplers for a given network
    
    https://networkx.github.io/documentation/stable/reference/generated/networkx.generators.degree_seq.configuration_model.html
    https://networkx.github.io/documentation/stable/reference/generated/networkx.generators.degree_seq.random_degree_sequence_graph.html#networkx-generators-degree-seq-random-degree-sequence-graph
    
    TEST CASES:
    https://github.com/networkx/networkx/blob/master/networkx/generators/tests/test_degree_seq.py
    """  
    # GET k
    sequence = nx.random_powerlaw_tree_sequence(100, tries=5000)
    # GET resampled graphs with the same k
    G = nx.configuration_model(sequence)
    actual_degrees = [d for v, d in G.degree()]
    actual_degrees == sequence
    # 
    G=nx.expected_degree_graph(z, selfloops=False)
	# LARGE: networkx/generators/tests/test_degree_seq.py: test_random_degree_sequence_large()
#--------------------------------
# [Bianconi et al.09], distance 
  
def get_classes(dij,aij,bins=10,directed=False):    
    """ digitize the values in 2d array
    Diagonal values set to -1.
    output
    -----
    """
    il = np.tril_indices(dij.shape[0],-1)
    dij_tril = dij[il]
    if np.any(dij_tril<=0) : raise ValueError('dij_tril=0')
    m,M=np.min(dij_tril),np.max(dij_tril)
    bin_edges = np.linspace(m,M+1,bins ) # NOTE: +1 is for compatibility with np.digitize(..,right=False)
    class_ = np.digitize(dij,bin_edges,right=False) -1
    #if np.any(class_<0): raise ValueError('class_<0')
    np.fill_diagonal(class_,-1)
    # compute B(d) the histogram
    #B_d,bin_edges=np.histogram(dij.flatten(),bin_edges); B_d = B_d/2.
    B_d = np.zeros(bins)
    if directed==False:
        for i in range(bins):
            B_d[i]=np.sum( aij * (class_==i)  )/2.
        return class_,B_d,bin_edges
    else: raise NotImplementedError()

def solve_lagrangian_distance(k,dij,class_dij,B_d,Nd,loops=1000,precision=10e-2):    
    """
    iterative solver for $k_i \propto \sum_{i \neq j} zi zj \sum W(dij) /(1+zi zj \sum W(dij))$
    distance-modulated configuration model
    adapted from Bianconi http://www.maths.qmul.ac.uk/~gbianconi/Entropy_distance.m
    http://mathesaurus.sourceforge.net/matlab-numpy.html
    
    Parameters:
    -----------
    k: array, shape=(n,)
    degree of undirected graph.
    
    dij: array, shape=(n,n)
    distances.
    
    B_d: array, shape=(n,)
    histogram
    
    Nd: int
    number of bins
    
    loops: int
    
    precision: float
    
    Outputs:
    ---------
    
    """
    if not isinstance(k, np.ndarray): raise ValueError('k must be np array')
    if not isinstance(dij, np.ndarray): raise ValueError('k must be np array')
    if not dij.ndim==2: raise ValueError('dij.ndim must be 2')
    n = k.shape[0]
    z = np.random.rand(n).reshape((n,1))
    W = np.random.rand(Nd).reshape((Nd,1))
    
    oldz = np.zeros(n).reshape((n,1))
    oldW = np.zeros(Nd).reshape((Nd,1))

    for kk in range(loops):
        bigW = np.zeros((n,n))
        for d in range(Nd): 
            bigW += W[d]*(class_dij==d)
        # update z
        U = (np.ones((n,1))*z.transpose()) * bigW
        D = np.ones((n,1)) + (z*z.transpose() ) * bigW
        U_D = U/D
        #s = np.sum( (U_D - np.diag(np.diag(U_D))).transpose() , axis=0 ).reshape((n,1))
        np.fill_diagonal(U_D,0.); s = np.sum( (U_D ).transpose() , axis=0 ).reshape((n,1))
        if np.any( np.isnan( U_D )): raise ValueError('nan in U_D')
        z = (k.transpose() / s.transpose()).transpose()
        z = np.maximum(z,10e-15 )            
        # update W
        B2 = np.zeros(Nd).reshape((Nd,1))
        for d in range(Nd):
            M = ((class_dij==d) * (z*z.transpose() ) )/( np.ones((n,n)) + z*z.transpose() * bigW )
            #B2[d] = ( np.sum( M - np.diag(np.diag(M)) )  )/2.
            np.fill_diagonal(M,0.); B2[d] = ( np.sum( M  )  )/2.
            if (B2[d] *B_d[d]) >0.0:
                W[d]= B_d[d] / B2[d]
                W[d] = np.maximum( W[d] ,1e-15 ) 
                W[d] = np.minimum( W[d] ,1e15 ) 
            else:
                W[d] = 0    
        # stop
        m1 = np.max( np.abs( (z.flatten()>0)*( 1-z.flatten()/(oldz.flatten() +1*(oldz.flatten()==0))  )  ) )
        m2 = np.max( np.abs( (W.flatten()>0)*( 1-W.flatten()/(oldW.flatten() +1*(oldW.flatten()==0))  )  ) )
        if (kk>30) and m1<precision and m2<precision:
            break
        oldz=z
        oldW=W
    # output    
    bigW = np.zeros((n,n))
    for d in range(Nd): 
        bigW += W[d]*(class_dij==d)  
    zzbigW = z*z.transpose() * bigW    
    pij = zzbigW /( np.ones((n,1)) +  zzbigW)          
    np.fill_diagonal(pij,0)
    return pij,z,W
    
def solve_lagrangian_k(k,loops=1000,precision=10e-2):
    """
    iterative solver for $k_i \propto \sum_{i \neq j} zi zj/(1+zi zj)$
    adapted from Bianconi http://www.maths.qmul.ac.uk/~gbianconi/Entropy_distance.m
    http://mathesaurus.sourceforge.net/matlab-numpy.html
    """
    if not isinstance(k, np.ndarray): raise ValueError('k must be np array')
    n = k.shape[0]
    z = np.random.rand(n).reshape((n,1))
    oldz = np.zeros(n).reshape((n,1))
    for kk in range(loops):
        for kkk in range(10):
            U = np.ones((n,1))*z.transpose()
            D = np.ones((n,1)) + z*z.transpose()
            U_D = U/D
            s = np.sum( (U_D - np.diag(np.diag(U_D))).transpose() , axis=0 ).reshape((n,1))
            z = (k.transpose() / s.transpose()).transpose()
            z = np.maximum(z,10e-15 )            
        if np.max( np.abs( (z.flatten()>0)*( 1-z.flatten()/(oldz.flatten() +1*(oldz.flatten()==0))  )  ) ) <precision: break
        oldz=z
    return z    
        
        
# -------------------------------------------------------
# EGM Enhanced gravity model

def wij_avg_UEGM(pij,yij):
    """
    compute $<w_{ij}>_{UEGM}$
	
    [Almog et al. 15a] eq.(17)
    """
    return pij/(1-yij)

def pij_UEGM(xij,yij):
    """
    compute $p_{ij}=<a_{ij}>_{UEGM}$
	
    [Almog et al. 15a] eq.(16)
    """
    prod = xij*yij
    return prod/(1-yij+prod)
	

def x_y_UEGM_GDP(s, delta,c,alpha,gamma,R):
    """
    compute x,y from the params delta,c,alpha,gamma.

    The expressions:
    F= c (s_i s_j)^alpha R_{ij}^{-\gamma}
    G = \delta s_i s_j
    
    are inserted in the general expression  [Almog et al. 15a] eq.(20-21):
    $x_{ij}=\frac{G}{F-1}$
    $y_{ij}=\frac{F-1 }{F }$
    """ 
    # compute F,G
    SS = np.outer(s,s)
    SS_alpha = SS**alpha
    R_gamma=R**(-gamma)
    F = c*SS_alpha * R_gamma
    G = delta *SS
    # compute x_{ij},y_{ij}
    x_ij = G/(F-1.)
    y_ij = (F-1.)/F
    np.fill_diagonal(x_ij,0.)
    np.fill_diagonal(y_ij,0.)
    return x_ij,y_ij
	

def eq_UEGM_GDP(h,W,A,s,R):
    """
    equations to be solved, solve [Almog et al. 15a] eq.(23):
    $\sum_{ij} \Big(  \frac{w_{ij}-a_{ij}}{F_{\phi}-1} - \frac{w_{ij}}{F_{\phi}} \Big) \vec{\nabla}F= \vec{0} $

    in the particular case:
    $F= c (s_i s_j)^alpha R_{ij}^{-\gamma}$
    $G = \delta s_i s_j$
    
    $F_\phi = c(GDP)^\alpha R^{-\gamma}$
    $\frac{\partial F}{\partial c} = (GDP)^\alpha R^{-\gamma}$
    $\frac{\partial F}{\partial \alpha} = c log(G) (GDP)^\alpha R^{-\gamma}$
    $\frac{\partial F}{\partial \gamma} = -c log(R) (GDP)^\alpha R^{-\gamma}$
    """
    c = h[0] ; alpha= h[1] ; gamma = h[2]
    # 
    SS = np.outer(s,s)
    SS_alpha = SS**alpha
    R_gamma=R**(-gamma)
    F = c*SS_alpha * R_gamma
    dL = (W-A)/(F-1)-W/F
    dL_dc = dL * SS_alpha * R_gamma
    dL_dalpha = dL *np.log(SS)*SS_alpha*R_gamma # "*c" can be dropped
    dL_dgamma= dL*SS_alpha*np.log(R)*R_gamma  # "*(-c)" can be dropped 
    # compute \sum_{i<j}	
    err_c = np.sum(np.tril(dL_dc,-1) ) 
    err_alpha = np.sum(np.tril(dL_dalpha,-1) ) 
    err_gamma = np.sum(np.tril(dL_dgamma,-1) ) 
    return np.array((err_c,err_alpha,err_gamma))

def solve_UEGM(W,A,s,R,L,z0=None,method='hybr',jac=None,options=None,
				fun=eq_UEGM_GDP):
    """ 
    solve [Almog et al. 15a] eq.(23-24) 
    
    parameters:
    ----------
    W: array, shape(n,n)
    weight matrix
    
    A:array, shape(n,n)
    adjacency matrix
    
    s: array, shape(n,)
    strength vector
    
    R: array, shape(n,n)
    distance matrix
    """
    if np.any(np.isnan(s)): raise ValueError('s contains nan')
    #n = s.shape[0]
    #args = (s,n, L)
    # solve [Almog et al. 15a] eq.(24)  
    sol_G=solve_UDCGM(s,L=L,z0=None,jac=None)
    # solve [Almog et al. 15a] eq.(23) 
    args = (W,A,s,R)
    if z0 is None:
        z0 = [1,1,1]
    sol_F  = root(fun=fun, args=args, x0=z0,method=method,jac=jac,options=options)
    return sol_F,sol_G

# -------------------------------------------------------
# UGM Undirected gravity model
def wij_UGM(s):
    """ 
    
    NB:no distance here. see [Cimini et al. 15] eq. (1)
    (see [Almog et al 15a] arxiv:1506.00348 for Enhanced Gravity Model of WTN
    with distance) 
    """
    xixj = np.outer(s,s)
    W = np.sum(s)
    return xixj/(2.*W)


# -------------------------------------------------------
# UFiGM Fitness Induced Undirected gravity model   

def solve_FiUGM(k,dij,x0=None,method="BFGS",jac=None,fun='exp'):
    """ 
    """
    if np.any(np.isnan(k)): raise ValueError('k contains nan')
    twoL = np.sum(k)
    n = k.shape[0]
    args = (k.astype(np.float),dij,n, twoL)
    # set bounds and initial condition
    if x0 is None:
        x0 = np.array([0.1,1.])
    opt={'gtol': 1e-06, 'disp':True, 'maxiter':1000}
    #sol  = root(fun=ficm_cy._func_FiUGM, args=args, x0=x0,method=method,jac=jac)
    if fun=='exp':
            f= ficm_cy._func_FiUGM
    elif fun=='pow':
            f= ficm_cy._func_FiUGM_pow
    else: raise ValueError('unknown fun')        
    sol  = minimize(fun=f, args=args, x0=x0,method=method,jac=jac,#options=opt,
                    bounds = [(0,None),(0,None)]
                    )
    return sol

def pij_FiUGM(k,dij,u,v,fun='exp'):
    """
    [Squartini et al. 14] in "Jan Tinbergen’s Legacy for Economic Networks:
    From the Gravity Model to Quantum Statistics"
    in F. Abergel et al. (eds.), Econophysics of Agent-Based models,New Economic Windows, DOI 10.1007/978-3-319-00023-7_9,
  
    see eq(9.18)
    """
    if not np.isscalar(u): raise ValueError('u must be scalar')
    if not np.isscalar(v): raise ValueError('v must be scalar')
    if u<=0: raise ValueError('u must be >0')
    if v<=0: raise ValueError('v must be >0')
    if fun=='exp':
        xixj = np.outer(k,k)*np.exp(-v*dij)            
    elif fun=='pow':
        xixj = np.outer(k,k)*np.power(dij,v)        
    else: raise ValueError('unknown fun')            
    P = u*xixj/(1.+u*xixj)
    np.fill_diagonal(P,0.)
    return P
    
# -------------------------------------------------------
# DDCGM directed degree-corrected gravity model


# -------------------------------------------------------
# UDCGM undirected degree-corrected gravity model


def pij_UDCGM(s,z):
    """
    [Squartini et al. 16] eq()
    """
    if not np.isscalar(z): raise ValueError('z must be scalar')
    if z<=0: raise ValueError('z must be >0')
    xixj = np.outer(s,s)
    P = z*xixj/(1.+z*xixj)
    np.fill_diagonal(P,0.)
    return P

def sample_wij_UDCGM(P,s,nsamp=1,correction=True):
    """ 
    Wtot = \sum_{i<j} w_{ij} = \sum_i s_i /2.

	TODO: slow, improve
    """
    if not P.shape[0]==s.shape[0]: raise ValueError('dimension mismatch')
    n = P.shape[0]
    Wtot = np.sum(s)/2.
    # uncorrected
    Wij = np.outer(s,s)/(2*Wtot)
    
    # correction
    if correction:
        # correction at order 2, [Squartini et al. 16] eq.(12)		
        s_sq = s**2
        u = np.sum(s_sq)*np.ones(s.shape[0])-s_sq
        Wij_correc = np.outer(s_sq,s_sq/u)/(2*Wtot)
        Wij = (Wij+Wij_correc)/P
    else:
	    Wij = Wij/P		    
    # ??
    Wij_samples = np.zeros((n,n,nsamp))
    # mc sample
    for i in range(nsamp):
        Wij_cp=Wij.copy()
        # bernoulli random var
        R = np.random.rand(n,n)
        idx = R>P
        Wij_cp[idx]=0. 
        Wij_samples[:,:,i]= Wij_cp
    return Wij_samples

def sample_aij_UDCGM(wij_sample,nsamp, naive=False):
    """ 
    """
    if naive:
        aij_sample = np.zeros(wij_sample.shape)
        nsamp = aij_sample.shape[2]
        for i in range(nsamp):
            aij_sample[:,:,i] = 1*(wij_sample[:,:,i]>0)
        return aij_sample    
    else:
        return 1*(wij_sample>0)

def sample_k_UDCGM(aij_sample,nsamp, naive=False):
    """
    TODO: slow, improve
    """
    if naive:
        n = aij_sample.shape[0]
        ki_sample = np.zeros((n,nsamp))
        for i in range(nsamp):
            ki_sample[:,i] = aij_sample[:,:,i].sum(axis=0)
        return ki_sample    
    else:
        return aij_sample.sum(axis=0)    

def sample_knn_UDCGM(aij_sample,ki_sample,nsamp):
    """
    TODO: slow, improve
    """
    knn_sample = np.zeros(ki_sample.shape)
    for i in range(nsamp):
        knn_sample[:,i] = knn_avg_UBCM( aij_sample[:,:,i], ki_sample[:,i])
    return knn_sample                

def sample_s(wij_sample,nsamp, naive=False):
    """
    TODO: slow, improve
    """
    if naive:
        n = aij_sample.shape[0]
        si_sample = np.zeros((n,nsamp))
        for i in range(nsamp):
            si_sample[:,i] = wij_sample[:,:,i].sum(axis=0)
        return si_sample    
    else:
        return wij_sample.sum(axis=0)
        
def sample_snn(aij_sample,ki_sample,si_sample,nsamp):
    """
    TODO: slow, improve
    """
    snn_sample = np.zeros(si_sample.shape)
    for i in range(nsamp):
        snn_sample[:,i] = snn( aij_sample[:,:,i] ,ki_sample[:,i],si_sample[:,i]) 
    return snn_sample 
            

def aij_avg_UDCGM(wij_sample):	
    aij_sample = 1.*(wij_sample>0)
    return np.mean(aij_sample,axis=2)
    
def wij_avg_UDCGM(wij_sample):
    """
    mc estimation of <w_ij> and p_ij
    """
    return np.mean(wij_sample,axis=2)
	

def solve_UDCGM(s,k=None,z0=None,method='hybr',jac=None, L=None):
    """ 
    """
    if np.any(np.isnan(s)): raise ValueError('s contains nan')
    if L is None:
        if k is None: raise ValueError('k must be given if L is None')
        if np.any(np.isnan(k)): raise ValueError('k contains nan')
        L = np.sum(k)
    n = s.shape[0]
    args = (s,n, L)
    # set bounds and initial condition
    if z0 is None:
        z0 = 0.5
    sol  = root(fun=ficm_cy._func_UDCGM, args=args, x0=z0,method=method,jac=jac)
    return sol

def hist_by_dij(class_dij,aij_sample,classes):
    """
    bin aij_sample per label, encoded in class _dij
    
    Parameters:
    -----------
    class_dij: array, shape=(n,n)
    distance bin of each element in (n,n) array.
    
    aij_sample: array, shape=(n,n,nsamp)
    samples
    
    nbins: int
    
    output:
    -------
    histbydij: array, shape=(nbins,)
    histogram per distance bin
    """
    #classes = np.sort(np.unique(class_dij))
    nclass=classes.shape[0]
    #if classes.shape[0] != nbins: raise ValueError("classes.shape[0] != nbins")
    histbydij = np.zeros(nclass)
    for i,c in enumerate(classes):
        idx =(class_dij==c)
        histbydij[i]=aij_sample[idx,:].sum()
    return histbydij    

# --------------------------------------------------------
# UECM (normal, and two-step [Almog et al. 15b])

def sample_UECM_two_step(pij_ts,wij_avg):
    """ """
    pass

def pij_ts_from_z(z):
    """ """
    zz = np.outer(z,z)
    np.fill_diagonal(zz,0)
    return zz/(1.+zz)

def wij_avg_from_pij_y(pij_ts,y):
    """ """
    yy = np.outer(y,y)
    np.fill_diagonal(yy,0)
    return pij_ts/(1.-yy)

def solve_UECM_two_step(k,s,pij_ts=None,solve_wij='iter',loops=1000,precision=10e-2):
    """ two-step algorithm [Almog et al. 15b]"""
    if pij_ts is None:
        # solve for $z_i$ in $p^{ts}_{ij}=<a_{ij}> = \frac{z_i z_j}{1+z_i z_j}$
        z = solve_lagrangian_k(k,loops=loops,precision=precision)
        pij_ts = pij_ts_from_z(z)
    # solve for $y_i$ in $<w_{ij}> = \frac{ p^{ts}_{ij} }{1-y_i y_j}$
    if solve_wij=='iter':
        y,max_, success=solve_iter_UECM_two_step_wij(pij_ts,s,loops=loops,precision=precision)
        if not success: warnings.warn('solve_iter_UECM_two_step_wij did not converge')
    elif solve_wij=='root':
        y,success = solve_root_UECM_two_step_wij(pij_ts,s)
        if not success: warnings.warn('solve_root_UECM_two_step_wij did not converge')        
    elif solve_wij=='opt':
        y,success = solve_opt_UECM_two_step_wij(pij_ts,s,method='BFGS')
        if not success: warnings.warn('solve_opt_UECM_two_step_wij did not converge')      
    else: raise ValueError('unknown solver for wij')    
    wij_avg = wij_avg_from_pij_y(pij_ts,y)
    return pij_ts,wij_avg

def eq_UECM_two_step(y,pij_ts,s):
    """ """
    wij_avg = wij_avg_from_pij_y( pij_ts, y)
    err_s = np.sum(wij_avg, axis=0) - s
    return err_s

def solve_opt_UECM_two_step_wij(pij_ts,si,method='BFGS',y0=None,jac=None):
    n = si.shape[0]
    if not is_matrix(pij_ts) : raise ValueError('pij_ts must be a matrix')
    if not n==pij_ts.shape[0]: raise ValueError('pij_ts and si have incompatible size')
    if not is_symmetric(pij_ts): raise ValueError('pij_ts must be symmetric')
    # set bounds and initial condition
    if y0 is None:
        y0 = 0.1*np.ones(n)
    # find the root
    def f(y,pij,s):
        err = eq_UECM_two_step(y,pij,s)
        return np.sum(err*err)
    def jac_def(y,pij,s):
        yy = np.outer(y,y)
        np.fill_diagonal(yy,0)
        x = -(pij_ts/(1.-yy)**2 ) * np.outer(y,np.ones(n))
        return np.sum(x, axis=0)
    if jac is None:    
        sol = minimize(f, y0,jac=jac_def, args=(pij_ts,si),method=method)    
    else:    
        sol = minimize(f, y0,jac=jac, args=(pij_ts,si),method=method)
    if not sol.success: print(sol)
    return sol.x,sol.success

def solve_root_UECM_two_step_wij(pij_ts,si,y0=None,method='hybr',jac=None):
    n = si.shape[0]
    if not is_matrix(pij_ts) : raise ValueError('pij_ts must be a matrix')
    if not n==pij_ts.shape[0]: raise ValueError('pij_ts and si have incompatible size')
    if not is_symmetric(pij_ts): raise ValueError('pij_ts must be symmetric')
    # set bounds and initial condition
    if y0 is None:
        y0 = 0.1*np.ones(n)
    # find the root
    sol = root(eq_UECM_two_step, y0,jac=jac, method=method, args=(pij_ts,si))
    if not sol.success: print(sol)
    return sol.x,sol.success

def solve_iter_UECM_two_step_wij(pij,si,loops=1000,precision=10e-2):
    """
    iterative solver for $<w_ij>= pij/(1-yi yj)$ in the two-step UEGM model.
    [Almog et al. 15b] arxiv:1512.02454
    
    Algorithm adapted from Bianconi http://www.maths.qmul.ac.uk/~gbianconi/Entropy_distance.m
    see solve_lagrangian_k()
    matlab/numpy conversion: http://mathesaurus.sourceforge.net/matlab-numpy.html
    """
    if not is_matrix(pij) : raise ValueError('pij_ts must be a matrix')
    if not is_symmetric(pij): raise ValueError('pij_ts must be symmetric')

    n = pij.shape[0]
    y = np.random.rand(n).reshape((n,1))
    oldy = np.zeros(n).reshape((n,1))
    for kk in range(loops):
        for kkk in range(10):
            U = pij
            D = np.ones((n,1)) - y*y.transpose()
            U_D = U/D
            sum_ = np.sum( (U_D - np.diag(np.diag(U_D))).transpose() , axis=0 ).reshape((n,1))
            y = (si.transpose() / sum_.transpose()).transpose()
            y = np.maximum(y,10e-15 )            
        max_ = np.max( np.abs( (y.flatten()>0)*( 1-y.flatten()/(oldy.flatten() +1*(oldy.flatten()==0))  )  ) )     
        if max_<precision: break
        oldy=y
    return y,max_, max_<precision   

"""
from sklearn.decomposition import NMF
def solve_nnmf_UECM_two_step_wij(wij,pij_ts,si):
    # symNMF
    #Kuang, D., Yun, S., Park, H.: SymNMF: nonnegative low-rank approximation of a similarity matrix for graph
    #clustering. Journal of Global Optimization 62(3), 545–574 (2014). doi:10.1007/s10898-014-0247-2
    # di.ens.fr/aspremon symANLS symHALS https://github.com/RaduAlexandruDragomir/QuarticLowRankOptimization
    #solve wij= pij_ts/(1-yiyj)
    # <=> solve 1-pij/wij=yiyj
    #https://github.com/dakuang/symnmf
    #solve wij= pij_ts/(1-yiyj)
    model = NMF(n_components=1, init='random', random_state=0)
    W = model.fit_transform(X)
    H = model.components_
"""    
    

def solve_ml_UECM(k,s,x0=None,method='Newton-CG', bounds=None,jac=None):
    """
    solve UECM by maximum likelikood 
    https://docs.scipy.org/doc/scipy/reference/tutorial/optimize.html#trust-region-constrained-algorithm-method-trust-constr
    """
    n = 2*k.shape[0]
    # set bounds and initial condition
    if x0 is None:
        x0 = 0.1*np.ones(n)
    #lb = np.zeros(n)
    #ub=np.inf*np.ones(n)
    #ub[idx]=0
    #bounds = Bounds(lb,ub)        
	# minimize
    args = (k,s,n)
    sol  = minimize(fun=cm_cy.like_UECM, x0=x0, args=args, bounds=bounds,
					method=method,jac=jac)
    return sol

def eq_UECM(h):
    """
    equations in [Mastandrea et al.14] eq.9-10
        
    modification of the function layerprob written by R.Krantz et al.
    """
    n = h.shape[0]
    m = int(n/2)
    x = h[0:m]
    y = h[m:n]
    xy = x*y
    XY = np.outer(xy, xy)
    YY = 1.-np.outer(y,y)
    D = 1./(YY+XY)
    K = XY * D
    S = K/YY
    np.fill_diagonal(K,0.)
    np.fill_diagonal(S,0.)
    return K,S

"""
def jac_UECM(h,dum1,dum2):
    
    #jacobian of the equations in [Mastandrea et al.14] eq.9-10
        
    #modification of the function layerprob written by R.Krantz et al.
    
    #NB: dum1,dum2 are not used
    
    n = h.shape[0]
    m = int(n/2)
    x = h[0:m]
    y = h[m:n]
    xy = x*y
    xy_2 = x*(y**2)
    XY = np.outer(xy, xy)
    YiXjYj = np.outer(y, xy)
    YY = 1.-np.outer(y,y)
    D = 1./(YY+XY)
    jac_K = YiXjYj*D + np.outer(xy_2, xy**2)*(D**2)
    jac_S = 1/YY * jac_K
    np.fill_diagonal(jac_K,0.)
    np.fill_diagonal(jac_S,0.)
    jac_k = np.sum(jac_K, axis=0) 
    jac_s = np.sum(jac_S, axis=0) 
    return np.hstack((jac_k,jac_s))
"""

def layereq_UECM(h,k,s):
    """
    modification of the function layereq written by R.Krantz et al.
    """
    K,S= eq_UECM(h)
    err_k = np.sum(K, axis=0) - k
    err_s = np.sum(S, axis=0) - s
    return np.hstack((err_k,err_s))

def solve_eq_UECM(k,s,h0=None,jac=None, method='hybr',options=None):
    """
    solve UECM by root-finding
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.root.html#scipy.optimize.root
    solver-specfic options: https://docs.scipy.org/doc/scipy/reference/optimize.root-lm.html#optimize-root-lm
    """
    n = 2*s.shape[0]
    # set bounds and initial condition
    if h0 is None:
        h0 = 0.1*np.ones(n)
    # find the root
    sol = root(layereq_UECM, h0,jac=jac, method=method, args=(k,s), options=options)
    return sol

def wij_avg_UECM(h):
    """ 
    [SG17] 4.7

    p_ij = \frac{x_i x_j (1-y_i y_j) Li_{-\gamma}(y_i y_j)}{1-y_i y_j + x_i x_j y_i y_j}
	
    with $\gamma=0$
	
    mpmath.polylog  doesn't support ndarray !!!
    other implementation of polylog: https://github.com/scipy/scipy/pull/84
	"""
    n = h.shape[0]
    m = int(n/2)
    x = h[0:m]
    y = h[m:n]
    XX = np.outer(x,x)
    YY = np.outer(y,y)    
    W = XX * (1.-YY) /  (1.-YY+XX*YY)
    # mpmath.polylog  doesn't support ndarray !!!
    LI = np.zeros((m,m))
    for i in range(m):
        for j in range(m):
             LI[i,j]=mpmath.polylog(YY[i,j],-1.)		
    W = W*LI
    # drop diagonal
    np.fill_diagonal(W,0.)
    return W
	
def pij_UECM(h):	
    """ 
    [SG17] 4.7
    
    p_ij = \frac{(1-y_i y_j)}{1-y_i y_j + x_i x_j y_i y_j}
    
    """
    n = h.shape[0]
    m = int(n/2)
    x = h[0:m]
    y = h[m:n]
    XX = np.outer(x,x)
    YY = np.outer(y,y)    
    P =(1.-YY) /  (1.-YY+XX*YY)	
    np.fill_diagonal(P,0.)
    return P
# --------------------------------------------------------
# UWCM Undirected Weighted Configuration Model

def pij_UWCM(x):
    """
    T. Squartini, D. Garlaschelli, New. J. Phys. 13, 083001 (2011).
    pij = x_i x_j
    """	
    P = np.outer(x, x)
    # remove diagonal
    n = x.shape[0]
    np.fill_diagonal(P,0.)
    """# remove negative values
    neg = np.where(P<0)
    P[neg]=0.
    # remove values >1
    thre = np.where(P>=1.)
    P[thre]=0.99"""
    return P

def wij_avg_UWCM(P):
    """
    T. Squartini, D. Garlaschelli, New. J. Phys. 13, 083001 (2011).
    <wij> = x_i x_j/( 1-x_i x_j)
    """	
    return P/(1-P)


def eq_UWCM(x):
    """
    modification of the function layerprob written by R.Krantz et al.

    cf [Squartini et al.14] eq.(33)
    """
    M = np.outer(x, x)
    P = M/(1-M)             # compare to pij_UBCM
    #zeros = np.where(P==0)
    #P[zeros] = 0.0000000001
	# remove diagonal	
    n = x.shape[0]
    P[np.diag_indices(n)]=0
    return P
    	
def layereq_UWCM(x,s):
    """
    modification of the function layereq written by R.Krantz et al.
    Ruben Krantz, Valerio Gemmetto, Diego Garlaschelli
    "Maximum-Entropy Tools for Economic Fitness and Complexity"
    Entropy 2018, 20, 743;  doi:10.3390/e20100743
    
    cf [Squartini et al.14] eq.(33)
    """
    S= eq_UWCM(x)
    flist = np.sum(S, axis=0) - s
    return flist

def solve_eq_UWCM(s, method='hybr'):
    """
    solve UWCM by root-finding
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.root.html#scipy.optimize.root
    """
    n = s.shape[0]
    # set bounds and initial condition
    idx, = np.where(s==0)
    x0 = 0.1*np.ones(n)
    x0[idx]=0
    # find the root
    sol = root(layereq_UWCM, x0, method=method, args=(s))
    return sol

def sample_UWCM(P):
    """
    after WCM_sampling.m in [Max&Sam]
    slow, do it in cython
    
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.geom.html#scipy.stats.geom
    """
    n=P.shape[0]
    W=np.zeros((n,n))
    for i in range(n):
        for j in range(i+1,n):	
            w = geom.rvs(1.-P[i,j]) -1 #!!!!!!!!!!!!!!!!!!!!!!!
            #w = geom.rvs(1.-P[i,j]) 
            W[i,j] = w
            W[j,i] = w
    return W

"""
def sample_avg_UWCM(P,nsamp):
    #compute sample average for strength and weights in the UWCM model
    s_avg = np.zeros(P.shape[0])    
    W_flat_avg = np.zeros(P.shape[0]**2)
    for i in range(nsamp):		
        W= sample_UWCM(P)
        s_avg += np.sum(W,axis=0)
        W_flat_avg += W.flatten()
    s_avg /= nsamp 
    W_flat_avg /= nsamp 
    return s_avg, W_flat_avg
"""
def sample_avg_UWCM(Wsamp):
    #compute sample average for strength and weights in the UWCM model
    if not Wsamp.ndim==3: raise ValueError('Wsamp.ndim !=3 ')
    if not Wsamp.shape[0]==Wsamp.shape[1]: raise ValueError('Wsamp[:,:,i] not square')
    nsamp = Wsamp.shape[2]
    
    s_avg = np.zeros(Wsamp.shape[0])    
    W_flat_avg = np.zeros(Wsamp.shape[0]**2)
    for i in range(nsamp):		
        W= Wsamp[:,:,i]
        s_avg += np.sum(W,axis=0)
        W_flat_avg += W.flatten()
    s_avg /= nsamp 
    W_flat_avg /= nsamp 
    return s_avg, W_flat_avg
# --------------------------------------------------------
# UBCM+dist Undirected Binary Configuration Model with distance


# --------------------------------------------------------
# UBCM Undirected Binary Configuration Model

"""
def pij_UBCM(x):
    #slow, do it in cython
    n=x.shape[0]
    P=np.zeros((n,n))
		
    for i in range(n):
        for j in range(i+1,n):
            P[i,j] =(x[i]*x[j])/(1. + (x[i]*x[j]))
    return P
"""
	
def sample_UBCM(P):
    """
    slow, do it in cython
    
    """
    n=P.shape[0]
    A=np.zeros((n,n))
    for i in range(n):
        for j in range(i+1,n):	
            z = np.random.rand()
            if z < P[i,j]:
                A[i,j] = 1
                A[j,i] = 1
    return A

def samples_UBCM(P, rnd_mat):
    """
    slow, do it in cython
    
    Parameters:
    -----------
    P: array, shape=(n,n)
    probability matrix
    
    rnd_mat: array, shape=(n,n,nsamp)
    random sample
    """
    n,m=P.shape
    if not n==m: raise ValueError('wrong dim P')
    if not P.ndim ==2: raise ValueError('ndim P != 2')
    if not rnd_mat.ndim ==3: raise ValueError('ndim rnd_mat != 3')
    if not rnd_mat.shape[0:2]==(n,n): raise ValueError('wrong dim rnd_mat')
    nsamp = rnd_mat.shape[2]
    A=np.zeros((n,n,nsamp))
    
    for i in range(n):
        for j in range(i+1,n):	
            z = rnd_mat[i,j,:]
            v=1*( z < P[i,j])
            A[i,j,:] = v
            A[j,i,:] = v
    return A

	
def pij_UBCM(x):
    """
    modification of the function layerprob written by R.Krantz et al.

    
    cf [Squartini et al.14] eq.(9)
    """
    M = np.outer(x, x)
    P = M/(1+M)
    zeros = np.where(P==0)
    n = x.shape[0]
    P[np.diag_indices(n)]=0
    P[zeros] = 0.0000000001
    return P
    	
def layereq_UBCM(x,k):
    """
    modification of the function layereq written by R.Krantz et al.
    Ruben Krantz, Valerio Gemmetto, Diego Garlaschelli
    "Maximum-Entropy Tools for Economic Fitness and Complexity"
    Entropy 2018, 20, 743;  doi:10.3390/e20100743
    
    cf [Squartini et al.14] eq.(9)
    """
    P = pij_UBCM(x)
    flist = np.sum(P, axis=0) - k
    return flist

def solve_eq_UBCM(k, method='hybr',x0=None):
    """
    solve UBCM by root-finding
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.root.html#scipy.optimize.root
    """
    n = k.shape[0]
    # set bounds and initial condition

    if x0 is None:
        idx, = np.where(k==0)
        x0 = 0.9*np.ones(n)
        x0[idx]=0
    # find the root
    sol = root(layereq_UBCM, x0, method=method, args=(k))
    return sol
	
def solve_ml_UBCM(k,x0=None,method='Newton-CG', bounds=None):
    """
    solve UBCM by maximum likelikood 
    https://docs.scipy.org/doc/scipy/reference/tutorial/optimize.html#trust-region-constrained-algorithm-method-trust-constr
    """
    n = k.shape[0]
    # set bounds and initial condition
    if x0 is None:
        idx, = np.where(k==0)
        x0 = 0.9*np.ones(n)
        x0[idx]=0
    #lb = np.zeros(n)
    #ub=np.inf*np.ones(n)
    #ub[idx]=0
    #bounds = Bounds(lb,ub)        
	# minimize
    args = (k,n)
    sol  = minimize(fun=cm_cy.like_UBCM, x0=x0, args=args, bounds=bounds,
					jac = cm_cy.jac_like_UBCM, 
					method=method)
    return sol

#-------------------------------------------------------	
# DBCM	

def pij_DBCM(x, nout, nin):
    # code R.Krantz
    M = np.outer(x[0:nout], x[nout:(nout+nin)])
    P = M/(1+M)
    zeros = np.where(P==0)
    P[zeros] = 0.0000000001
    return P

def layereq_DBCM(x, kout, kin, nout, nin):
    # code R.Krantz
    P = pij_DBCM(x, nout, nin)
    keninst = np.sum(P, axis=1) - kout
    kininst = np.sum(P, axis=0) - kin
    fninst = np.hstack((keninst, kininst))
    return fninst
	
def solve_eq_DBCM(kin,kout, method='lm',x0=None):
    """ """
    nin = kin.shape[0]
    nout = kout.shape[0]
    #Initial guess for hidden variables
    if x0 is None:
        x0 = np.empty((nin+nout))
        x0[0:nout] = kout/np.average(kout)
        x0[nout:(nout+nin)] = kin/np.average(kin)

    #Solve equations for hidden variables
    sol = root(layereq_DBCM, x0, args=(kout, kin, nout, nin), method = method)
    return sol

#----------------------------------------------------------	
# TOOLS 

# -------------
# DBCM
def k_avg_DBCM(P):
    """
    average degree of a binary directed graph model specified by P={p_ij}
	
    parameters:
    ----------
    P: array, shape=(n,n)
    the edge probability matrix
    """
    kin_avg = np.sum(P,axis=0)
    kout_avg = np.sum(P,axis=1)
    return kin_avg,kout_avg

def knn_avg_DBCM(P,kin,kout):
    """ 
    average nearest neighbor degree of a binary directed graph model
    specified by P={p_ij}.
	from matrix.
	
	[Squartini17] eq 3.9-10
	$k_i^{nn,out/out} =\frac{\sum_{j \neq i} a_{ij} k_j^{out} }{k_i^{out}}$
	$k_i^{nn,in/out} =\frac{\sum_{j \neq i} a_{ji} k_j^{in} }{k_i^{in}}$
	
    parameters:
    ----------
    P: array, shape=(n,n)
    the edge probability matrix
    
    kin: array, shape=(n,)
    average in-degree
	
    kout: array, shape=(n,)
    average out-degree

    ?????TODO: recode in C ; compare performance

    https://docs.scipy.org/doc/numpy/reference/maskedarray.generic.html
    """
    if not P.ndim==2: raise ValueError('P must be 2-dimensional')
    if not kin.ndim==1: raise ValueError('kin must be 1-dimensional')
    if not kout.ndim==1: raise ValueError('kout must be 1-dimensional')

    n = P.shape[0]
    knn_out = np.zeros(n)
    knn_in = np.zeros(n)
    for i in range(n):
        knn_out[i]= 1./kout[i] * np.dot(P[i,:], kout)			
        knn_in[i]= 1./kin[i] * np.dot(P[:,i], kin)			
    return knn_in,knn_out

# -------------
# UBCM

def clustering_avg(P):
	"""
	cf https://github.com/igraph/igraph/blob/master/src/triangles.c
    see network_tools.Triangles
	"""
	pass

def k_avg_UBCM(P):
	"""
	average degree of a binary undirected graph model specified by P={p_ij}
	from matrix.
	
    parameters:
    ----------
    P: array, shape=(n,n)
    the edge probability matrix
	"""
	return P.sum(axis=0)

def k_sigma_UBCM(P):
	"""
	std of the degree of a binary undirected graph model specified by P={p_ij}.

	Ref: [SG11] eq. (B.33)

    parameters:
    ----------
    P: array, shape=(n,n)
    the edge probability matrix
	"""	
	Q = P*(1.-P)
	return np.sqrt(Q.sum(axis=0))
	

def knn_avg_UBCM(P,k):
    """ 
    average nearest neighbor degree of a binary undirected graph model
    specified by P={p_ij}.
	from matrix.
	
    parameters:
    ----------
    P: array, shape=(n,n)
    the edge probability matrix
    
    k: array, shape=(n,)
    degree
	
    ?????TODO: recode in C ; compare performance

    https://docs.scipy.org/doc/numpy/reference/maskedarray.generic.html
    """
    if not P.ndim==2: raise ValueError('P must be 2-dimensional')
    if not k.ndim==1: raise ValueError('k must be 1-dimensional')

    n = P.shape[0]
    knn = np.zeros(n)
    mysum = P.sum(axis=0)
    for i in range(n):
        knn[i]= 1./k[i] * np.dot(P[i,:], mysum)			
    return knn


def knn_sigma_sample(A,directed=False):
    """
    empirical std of the average degree of neighbors for each vertex.
    NB1: analytic expression not available, fall back to numerical approx.
    NB2: SLOW for large graphs
	
    Parameters:
    ----------
    A: array, shape(n,n,nsamp)
    sampled adjacency matrices
	
    Output:
    ----------
    std_ann_per_vertex: shape(n,)
    empirical std of the average degree of neighbors for each vertex.
	
    see ig.Graph.knn
	
    """
    # check args
    if not A.ndim==3: raise ValueError('A must be 3-dimensional')
    n,m,nsamp=A.shape
    if not n==m: raise ValueError('A[:,:,i] must be square')
    if directed: 
        mode=ig.ADJ_DIRECTED
    else:	
        mode=ig.ADJ_UNDIRECTED
    # compute knn, for each samp (create igraph, compute knn)
    result = np.zeros((n,nsamp))
    for i in range(nsamp):
        g = ig.Graph.Adjacency(A[:,:,i].tolist(),mode=mode)
        ann_per_vertex,ann_per_deg = g.knn()
        result[:,i] = np.array(ann_per_vertex)
    # compute empirical std 
    std = result.std(axis=1)
    # replace nan by 0
    std[np.isnan(std)]=0.
    return std

def wknn_sigma_sample(W,directed=False):
    """
    empirical std of the wknn for each vertex.
    NB1: analytic expression not available, fall back to numerical approx.
    NB2: SLOW for large graphs
	
    Parameters:
    ----------
    W: array, shape(n,n,nsamp)
    sampled weighted adjacency matrices
	
    Output:
    ----------
    std_wknn_per_vertex: shape(n,)
    empirical std of the wknn for each vertex.
	
	
    """
    # check args
    if not W.ndim==3: raise ValueError('W must be 3-dimensional')
    n,m,nsamp=W.shape
    if not n==m: raise ValueError('W[:,:,i] must be square')
    # compute wknn, for each samp 
    result = np.zeros((n,nsamp))
    for i in range(nsamp):
        W_ = W[:,:,i]
        if not directed:
            s = np.sum(W_, axis=0)
            wknn_ = wknn(W_,s)
        else:
            raise NotImplementedError
        result[:,i] = wknn_
    # mask invalid    
    result_mask=ma.masked_invalid(result)    
    # compute empirical std 
    std = result_mask.std(axis=1)
    # replace nan by 0
	#std[np.isnan(std)]=0.
    return std
	
def sample_wknn(wij_sample,si_sample,nsamp):
    """
    TODO: slow, improve
    """
    wknn_sample = np.zeros(si_sample.shape)
    for i in range(nsamp):
        wknn_sample[:,i] = wknn( wij_sample[:,:,i] ,si_sample[:,i]) 
    return wknn_sample     
	
# --------------------------------------------------------



def snn_sigma_sample(W,directed=False):
    """
    empirical std of snn for each vertex.
    NB1: analytic expression not available, fall back to numerical approx.
	
    Parameters:
    ----------
    W: array, shape(n,n,nsamp)
    sampled weight matrices
	
    Output:
    ----------
    std_snn_per_vertex: shape(n,)
    empirical std of the snn for each vertex.
	
    """
    # check args
    if not W.ndim==3: raise ValueError('W must be 3-dimensional')
    n,m,nsamp=W.shape
    if not n==m: raise ValueError('W[:,:,i] must be square')
    if directed: 
        raise NotImplementedError
        
    # compute snn, for each samp 
    result = np.zeros((n,nsamp))
    A = 1*(W>0) 	
    #k = A.sum(axis=0)
    #s = W.sum(axis=0)
    for i in range(nsamp):
        #result[:,i] = snn(A[:,:,i],k[:,i],s[:,i])
        k = A[:,:,i].sum(axis=0)
        s = W[:,:,i].sum(axis=0)
        result[:,i] = snn(A[:,:,i],k,s)
    # mask invalid    
    result_mask=ma.masked_invalid(result)
    # compute empirical std 
    std = result_mask.std(axis=1)
    # replace nan by 0
    #std[np.isnan(std)]=0.
    return std


def wknn(W,s):
    """ 
    weighted ANND of a weighted undirected graph.
    from matrix.
    
	See [SG11] eq (19)
	$\tilde{k}^{nn}_i=\frac{ \sum_{j \neq i} \sum_{k \neq j} w_{ij} w_{jk} }{W \sum_{j \neq i} w_{ij} }$, see [SG11] (19)  
	with $W=\sum_{i<j} w_{ij}$
	
	NB: doesn't depend on a random ensemble. Can be used to compute
	wknn_avg_UWCM, if W is replaced by <W>.
	
	[SG11] Squartini, Garlaschelli  ...
	
    parameters:
    ----------
    W: array, shape=(n,n)
    the weight matrix
      
    s: array, shape=(n,)
    strength
	
    ?????TODO: recode in C ; compare performance
	
    """
    if not W.ndim==2: raise ValueError('W must be 2-dimensional')
    if not s.ndim==1: raise ValueError('s must be 1-dimensional')
    # compute Wtot
    Wtot = np.sum(W)/2.
    #n = W.shape[0]
    #wknn = np.zeros(n)
    # remove diagonal term
    np.fill_diagonal(W,0.)
    # 
    wknn = W.dot(s)
    wknn = wknn/s
    #wknn[np.isnan(wknn)] = 0.
    return wknn/Wtot
	

