# -*- coding: utf-8 -*-
"""

the Florida Bay food web (N=128)[38], 
the Italian interbank network (N=215) [34], 
the C. elegans neural network (N=265) [32],
a snapshot of the US airport network (N=332)

distances in python:
https://github.com/geopy/geopy
https://janakiev.com/blog/gps-points-distance-python/

"""
import networkx as nx
from igraph import *
import numpy as np
from cm_ml.cmtools import get_undirected_weighted_ig
from scipy.spatial import distance_matrix
import pandas as pd

try:
	from mplex.mplexio import read_ots_yrp_data_igraph
except:
	print('could not load mplex.mplexio')	

try:
	import cartopy.geodesic as gd
except:
	print('could not load cartopy.geodesic')	
try:
    from geopy.distance import geodesic
except:
    print('could not load geopy')	
    
def get_florida(backend='igraph'):
    if backend=='igraph':
        return get_florida_igraph()
    elif backend=='nx':	
        return get_florida_nx()
    else: raise ValueError('unknown backend')   
	

def get_trade_gleditsch_df(path="/home/aurelien/local/data/comtrade/gleditsch_trade/trade_dd.asc",directed=True,weight=False, country_code='COW'):
    """
    get data in [Gleditsch 02] as a DataFrame 
    
    Usage:
    ------
    >>df=get_trade_gleditsch_df(path,country_code='ISO')
    >>df.loc[2000]    # year

    trade_dd.asc    
    acra/b		Three letter acronym for state A/B	
    numa/b		Numeric id code for state A/B
    year		Year
    expab		Export A to B
    eabo		Export A to B origin code
    impab		Import A from B
    iabo		Import A from B origin code
    expba		Export B to A 		
    ebao		Export B to A origin code
    impba		Import B from A 
    ibao		Import B from A origin code

    REF:
    [Gleditsch 02] Gleditsch, Kristian S. 2002. "Expanded Trade and GDP Data," Journal of Conflict Resolution 46: 712-24. 
    http://ksgleditsch.com/exptradegdp.html
    """

    if directed:
        # read csv
        df = pd.read_csv(path,sep=' ')
        df = df[ [u'acra', u'acrb', u'year', u'impab']]
        if country_code=='COW':
            pass
        elif country_code=='ISO':    		   
            # TODO: REPLACE COW codes by iso
            # TODO: remove nan rows 
            df_name = get_gleditsch_country_name_iso(path="data/country_name_COW_to_iso.csv")       
            df_name=  df_name.rename(columns={"cowcodes": "acra", "iso3c": "acra_iso"})
            # first join: keep only names in name_list
            df = pd.merge(df,df_name,'left')
            df_name = get_gleditsch_country_name_iso(path="data/country_name_COW_to_iso.csv")
            df_name=  df_name.rename(columns={"cowcodes": "acrb", "iso3c": "acrb_iso"})
            df = pd.merge(df,df_name,'left')
            # drop
            df = df.drop(['acra','acrb'], axis=1)
        else: raise ValueError('unknown country_code')
        # set NaN to zero: use df.fillna instead
        #df.fillna(0, inplace=True)
        if not weight:
            df = df[ df['impab']>0]	            
        # pivot 
        if country_code=='COW':   
            return df.pivot_table(index=['year','acra'], columns='acrb',values='impab',fill_value=0)            	 
        elif country_code=='ISO':	
            return df.pivot_table(index=['year','acra_iso'], columns='acrb_iso',values='impab',fill_value=0)            	 			
    else: raise NotImplementedError

def get_trade_gleditsch_distance_matrix(year,path=None):
    """ """
    if not year>0: raise ValueError('year must be >0')
    if path is None:
        df = get_trade_gleditsch_df(directed=True,weight=True)
    else: df = get_trade_gleditsch_df(path,directed=True,weight=True)
    df_y= df.loc[year]    
    # get list of countries present that year 
    country_code= list(df_y.index.values)
    # get W,A
    W= df.loc[year,country_code].to_numpy()
    A=1*(W>0)
    return W,A,country_code

def get_gleditsch_country_name_iso(path="data/country_name_COW_to_iso.csv"):
    """get conversion table between COW country names and iso.
    
    The file is built using the following code, thanks to R package countrycode
    Arel-Bundock, Vincent, Nils Enevoldsen, and CJ Yetman, (2018). countrycode: An R package to convert country names and country codes. Journal of Open Source Software, 3(28), 848, https://doi.org/10.21105/joss.00848
    https://github.com/vincentarelbundock/countrycode
    
    install.packages('countrycode')
    library('countrycode')
    d= read.csv("/home/aurelien/local/data/comtrade/gleditsch_trade/trade_dd.asc",sep=" ")
    cowcodes = sort(unique(d$acra))
    df1 = data.frame(cowcodes)
    df1$iso3c   <- countrycode(df1$cowcodes, origin = "cowc", destination = "iso3c")
    #  "Some values were not matched unambiguously: CZE, FJI, GDR, GFR, KBI, RUM, RVN, TBT, YPR, YUG, ZAN"
    #seq(196)[is.na( df1$iso3c)]
	write.csv(df1,file="country_name_COW_to_iso.csv",row.names=FALSE)
    """  
    return pd.read_csv(path)  
    
def get_trade_gleditsch_nx():
	pass	
def get_trade_gleditsch_ig():
	pass		

def get_florida_nx():
	"""
	http://vlado.fmf.uni-lj.si/pub/networks/data/bio/foodweb/Florida.paj
	"""
	net = nx.read_pajek('data/Florida.paj')
	k=0
	s=0
	return net,k,s

def get_us_airport(path='data/USAir97.net',backend='igraph'):
    if not (backend in ['igraph','nx']): raise ValueError('unknown backend') 
    if backend=='igraph':
        g,k,s,A,W = get_us_airport_igraph(path)
        if np.all(s==0):
            print('error reading pajek file with igraph backend...')
        else:
            return  g,k,s,A,W
    # fallback to nx backend
    return get_us_airport_nx(path)
    

def get_us_airport_nx(path):
	"""	
	Weighted directed
		
	https://networkx.github.io/documentation/stable/auto_examples/drawing/plot_weighted_graph.html#sphx-glr-auto-examples-drawing-plot-weighted-graph-py
	
	http://vlado.fmf.uni-lj.si/pub/networks/data/
	http://vlado.fmf.uni-lj.si/pub/networks/data/mix/USAir97.net
	"""
	net = nx.read_pajek(path) 
	k=sorted(list(net.degree() ))
	#[(u, v,d['weight']) for (u, v, d) in net.edges(data=True)]
	
	#compute strength sequence
	s=sorted(list(net.degree(weight='weight')))
	# keep only the degree and the weight 
	W = nx.adjacency_matrix(net).todense()
	A = 1*(W>0)
	return net, [k_ for name,k_ in k], [s_ for name,s_ in s], A,W
	
def get_us_airport_igraph(path)	:
    """ 
    https://igraph.org/python/doc/igraph.Graph-class.html#get_adjacency
    """
    g = Graph.Read_Pajek(path)
    A = g.get_adjacency(type=GET_ADJACENCY_BOTH, attribute=None, default=0, eids=False)
    W = g.get_adjacency(type=GET_ADJACENCY_BOTH, attribute='weight', default=0, eids=False)
    k = np.array(g.degree())
    s = np.array(g.strength(weights='weight'))
    return g,k,s,np.array(A.data),np.array(W.data)

def get_us_airport_distance_igraph(path)	:
    """ 
    https://igraph.org/python/doc/igraph.Graph-class.html#get_adjacency
    """
    g,k,s,A,W = get_us_airport_igraph(path)
    x = np.array(g.vs['x'])
    y = np.array(g.vs['y'])
    xy=np.vstack((x,y)).transpose()
    Dij= distance_matrix(xy,xy)	
    return g,k,s,A,W,Dij

def distance_matrix_geo(x,y):
    """
    compute geodesic distance between x[i,:] and y[j,:] for all i,j
    """
    if not (isinstance(x,np.ndarray) and isinstance(y,np.ndarray)):
        raise ValueError('x and y must be np.ndarray')
    if not (x.shape[1]==2 and y.shape[1]==2):  raise ValueError('shape[1] must be =2')
    #k = gd.Geodesic() # defaults to WGS84        
    n=x.shape[0]
    
    R = np.zeros((n,n))
    for i in range( n ):
        for j in range( i+1,x.shape[0] ):		            
            #R[i,j]= k.inverse( [x[i],y[i]] , [x[j],y[j]]).base[0,0]/1000 			      
            #R[i,j]= k.inverse(  x[i,:] , y[j,:]  ).base[0,0]/1000 			      
            R[i,j] = geodesic( x[i,:] , y[j,:] ).km
    return R	

def get_comtrade_igraph(path, directed=True):
    """
    see https://data.tradestatistics.io
    """
    g = read_ots_yrp_data_igraph(path,directed=True,imports=True)
    if not directed:
        g= get_undirected_weighted_ig(g)
    A = g.get_adjacency(type=GET_ADJACENCY_BOTH, attribute=None, default=0, eids=False)
    W = g.get_adjacency(type=GET_ADJACENCY_BOTH, attribute='weight', default=0, eids=False)
    k = g.degree()
    k_ar=np.array(k)
    s = np.array(g.strength(weights='weight'))
    return g,k,s,np.array(A.data),np.array(W.data)

"""def get_comtrade_distance_matrix():
    
    g.vs['name'] 
   
    # make df from country_metada.json
    df1 = pd.read_json('data/comtrade_country_metadata.json')
    df1 = df1.loc[0:242,('country_iso', 'country_name_english')]
    # read country_geo.csv
    df2 = pd.read_csv('data/country_geo.csv',delimiter=';') 
    df2=df2.rename(columns={"name": "country_name_english"})
    df2=df2.iloc[:,1:] 
    # join
    df3=pd.merge(df1,df2,'left') 
    #df3[pd.isna(df3['latitude']) ] 
    # sort
    df3 = df3.sort_values(by='country_iso')   
    # compute interdistance
    x=df3.loc[:, ('latitude', 'longitude')].to_numpy()
    D=distance_matrix_geo(x,x)
    return D, df3['country_iso'].to_list()
 """
def get_comtrade_distance_matrix(country_name_iso, comtrade_metadata='data/comtrade_country_metadata.json',
								country_geo='data/country_geo.csv'):
    """
    get the inter-country distance matrix.
    Requires:  lat/lon for countries.
    """
    if not isinstance(country_name_iso,list): raise ValueError('country_name_iso must be a list')
    # make df from country_metada.json
    df0= pd.DataFrame({'country_iso':country_name_iso})
    df1 = pd.read_json(comtrade_metadata)
    df1 = df1.loc[0:242,('country_iso', 'country_name_english')]
    # first join: keep only names in name_list
    df1 = pd.merge(df0,df1,'left')
    # read country_geo.csv
    df2 = pd.read_csv(country_geo,delimiter=';') 
    df2=df2.rename(columns={"name": "country_name_english"})
    df2=df2.iloc[:,1:] 
    # 2nd join
    df3=pd.merge(df1,df2,'left') 
    #df3[pd.isna(df3['latitude']) ] 
    # sort
    #df3 = df3.sort_values(by='country_iso')   
    # compute interdistance
    x=df3.loc[:, ('latitude', 'longitude')].to_numpy()
    D=distance_matrix_geo(x,x)
    return D

def get_comtrade_distance_igraph(path,directed=True)	:
    """ 
    https://igraph.org/python/doc/igraph.GraphBase-class.html#permute_vertices
    """
    g = read_ots_yrp_data_igraph(path,directed=True,imports=True)
    if not directed:
        g= get_undirected_weighted_ig(g)
    #permute vertices so that labels are sorted    
    nam = g.vs['name']
    idx = np.argsort(nam) # ! argsort doesn't return a permutation vector
    idx_perm=[]
    for i in range(idx.shape[0]):
	    j = np.where( idx==i )[0][0]	
	    idx_perm.append(j)
    g_perm=g.permute_vertices(idx_perm)
    # 
    A = g_perm.get_adjacency(type=GET_ADJACENCY_BOTH, attribute=None, default=0, eids=False)
    W = g_perm.get_adjacency(type=GET_ADJACENCY_BOTH, attribute='weight', default=0, eids=False)
    k = g_perm.degree()
    k_ar=np.array(k)
    s = np.array(g_perm.strength(weights='weight'))
    # get Dij
    Dij= get_comtrade_distance_matrix(g_perm.vs['name'])
    Dij = Dij + Dij.transpose()
    return g,k,s,np.array(A.data),np.array(W.data),Dij
