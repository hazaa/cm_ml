# -*- coding: utf-8 -*-
"""
"""
"""from .deterrenceFunction import *
from .utilSpatialNullM import *
from .GraphModels import * """
from spaceCorrectedLouvainDC.Tools.deterrenceFunction import *  
#from spaceCorrectedLouvainDC.Tools.deterrenceFunctionDegree import * #AH
from spaceCorrectedLouvainDC.Tools.utilSpatialNullM import *
from spaceCorrectedLouvainDC.Tools.GraphModels import *
from spaceCorrectedLouvainDC.Tools.spatialNullModel import *

import math
from scipy.stats.stats import pearsonr
import warnings

ROOT = "/home/aurelien/local/git/extern/spaceCorrectedLouvainDC"

def test():
	plot=True
	flowFile = "{}/files/Global2011.txt".format(ROOT)
	distanceFile = "{}/files/distancesLyon.txt".format(ROOT)

	#obtain the observed network as a networkx object
	observedNetwork = readVelovFileAsNetworkX(flowFile)

	# set weights to 1
	for (u, v, wt) in observedNetwork.edges.data('weight'):
		if wt>0: observedNetwork[u][v]['weight'] = 1.0        

	#obtain the distance between nodes as a function that takes 2 nodes and return their distance
	# In little example like this one, precomputed because faster, but could be computed on the fly
	distancesBetweenNodes = Distances()
	distancesBetweenNodes.getDistanceFunctionVelov(distanceFile)

	#compute the null model of a spatial network. roundDecimal = -2 : bin every 100 unity (meters)
	(nullModel, distFunction) = getSpatialNullModel(observedNetwork,distancesBetweenNodes.getDistanceBetween,roundDecimal=-2,iterations=5,plot=plot)
	#(nullModel, distFunction) = getSpatialNullModelDegree(observedNetwork,distancesBetweenNodes.getDistanceBetween,roundDecimal=-2,iterations=5,plot=plot)
	#(nullModel, distFunction) = getSpatialNullModelExpertEtAlDegree(observedNetwork,distancesBetweenNodes.getDistanceBetween,roundDecimal=-2,plot=plot)

	#compute undirected version of null model
	graphOfNullModel = createnxGraphFromGraphModel(nullModel)

	#source, dest = '4041', '5006'; nullModel.getExpectedEdges( source, dest)

	# check consistence (\sum pij=\sum_aij, etc...)
	for (u, v, wt) in observedNetwork.edges.data('weight'):
		pij=nullModel.getExpectedEdges( source, dest)
		if  pij>1: print('pij>1')        
		if  pij<0: print('pij<0')        
    # compute degree, knn
    kin_mod = dict(graphOfNullModel.in_degree(weight="weight"))
    kout_mod = dict(graphOfNullModel.in_degree(weight="weight"))

    kin = dict(observedNetwork.in_degree()
    kout = dict(observedNetwork.in_degree()
# ------------------------------------------------------------
# BELOW NOT USED
    

def _estimateEISsIN(EISsIN, EISsOUT, INs, OUTs, deterrencefunc, distances, normalized=False):
	#EIS : estimated Intrinsic Strenght
	newEISsIN = {}
	sumIn = sum(INs.values())

	#for each node
	for nodeDest in INs:
		#compute how many interaction it receives
		sumReceived =0.0
		for nodeSource in EISsOUT:
			sumReceived+=deterrencefunc(distances(nodeSource,nodeDest))*EISsOUT[nodeSource]*INs[nodeDest]/sumIn

		#modify its "intrinsic degree" to receive the right number according to reference
		newEISsIN[nodeDest] = INs[nodeDest] / sumReceived * INs[nodeDest]
		#print(nodeDest,sumReceived,INs[nodeDest],newEISsIN[nodeDest])


	return newEISsIN

def _estimateEISsOUT(EISsIN, EISsOUT, INs, OUTs, deterrencefunc, distances, normalized=False):
	# EIS : estimated Intrinsic Strenght
	newEISsOUT = {}
	sumOut = sum(OUTs.values())

	# for each node
	for nodeSource in OUTs:
		# compute how many interaction it receives
		sumSent = 0.0
		for nodeDest in EISsIN:
			sumSent += deterrencefunc(distances(nodeSource, nodeDest)) * EISsIN[nodeDest] * OUTs[nodeSource]/sumOut

		# modify its "intrinsic degree" to receive the right number according to reference
		newEISsOUT[nodeSource] = OUTs[nodeSource] / sumSent * OUTs[nodeSource]

	return newEISsOUT

def getSpatialNullModelExpertEtAlDegree(originalNetwork,distances,roundDecimal,maximalDistance=100000,plot=False,printDebug=False):
	"""
	:param originalNetwork: the observed network for which we want the corresponding null model
	:param distances: a function that return the distance between two nodes of the provided graph
	:param roundDecimal: the rounding used to compute bins of the deterrence function. for a distance d=123.456 :
	 if roundDecimal=2, binned value is 123.46.
	 if roundDecimal=-2, binned value is 100
	:param maximalDistance: ignore in most cases, parameter of the deterence function to set an upper bound on the considered distances
	:param minValsBin: parameter of the deterrence function, minimum number of observations in a bin to consider it. (avoid abherent values for rare distances)
	:param plot: if True, plot the deterrence function before the doubly constrained process and at the end of the process
	:param printDebug: print at each step a trace of the current model: edit distance with original network, degree bias towards central nodes...
	slow down the process a lot, use only to understand or check that everything is going well.
	"""
	return getSpatialNullModelDegree(originalNetwork,distances,roundDecimal,maximalDistance=maximalDistance,plot=plot,iterations=0,printDebug=printDebug)


def getSpatialNullModelDegree(originalNetwork,distances,roundDecimal,maximalDistance=100000,minValsBin = 3,plot=False,iterations=5,printDebug=False):
	"""
	:param originalNetwork: the observed network for which we want the corresponding null model
	:param distances: a function that return the distance between two nodes of the provided graph
	:param roundDecimal: the rounding used to compute bins of the deterrence function. for a distance d=123.456 :
	 if roundDecimal=2, binned value is 123.46.
	 if roundDecimal=-2, binned value is 100
	:param maximalDistance: ignore in most cases, parameter of the deterence function to set an upper bound on the considered distances
	:param minValsBin: parameter of the deterrence function, minimum number of observations in a bin to consider it. (avoid abherent values for rare distances)
	:param plot: if True, plot the deterrence function before the doubly constrained process and at the end of the process
	:param iterations: number of iterations in the doubly constrained process
	:param printDebug: print at each step a trace of the current model: edit distance with original network, degree bias towards central nodes...
	slow down the process a lot, use only to understand or check that everything is going well.
	"""
	print("Computing the spatial null model with %s iterations"%(iterations))
	norm = False
	GraphModelOriginal = GraphModelAsnxGraph(originalNetwork)


	# compute in and out degrees
	"""
	INs = dict(originalNetwork.in_degree(weight="weight"))
	OUTs = dict(originalNetwork.out_degree(weight="weight"))
	"""
	INs = dict(originalNetwork.in_degree())   #AH
	OUTs = dict(originalNetwork.out_degree()) # AH


	# ---Print summary progress---------
	if printDebug:

		temporaryModel = ConfigurationModel(INs, OUTs)
		print("EDIT distance with a configuration model: %s" % temporaryModel.getEditDistWithGraphModel(GraphModelOriginal))

		(degreesConvergenceIN,degreesConvergenceOUT) = temporaryModel.getDegreesSimilarity(GraphModelOriginal)
		print("--convergence of degrees, IN: %s OUT: %s" % (degreesConvergenceIN, degreesConvergenceOUT))

		#_checkSystematicBiasPosition(distances, GraphModelOriginal, temporaryModel)

	# --------------------------------------------


	print("computing the original deterrence function")
	#deterrencefunc = deterrenceFunction()  
	deterrencefunc = deterrenceFunctionDegree() #AH
	deterrencefunc.deterrenceFunctionEstimation(INs, OUTs, originalNetwork, distances,
													  roundDecimals=roundDecimal, maximalDistance=maximalDistance, plot=plot,minVals = minValsBin)

	# ---Print summary progress---------
	if printDebug or iterations==0:
		temporaryModel = GravityModel(INs,OUTs,deterrencefunc.getDeterrenceAtDistance,distances)
		print(
			"EDIT distance with Simple Gravity model: %s" % temporaryModel.getEditDistWithGraphModel(GraphModelOriginal))
		(degreesConvergenceIN, degreesConvergenceOUT) = temporaryModel.getDegreesSimilarity(GraphModelOriginal)
		print("--convergence of degrees, IN: %s OUT: %s" % (degreesConvergenceIN, degreesConvergenceOUT))
		#_checkSystematicBiasPosition(distances, GraphModelOriginal, temporaryModel)

	# --------------------------------------------




	EISsIN=INs
	EISsOUT=OUTs

	for i in range(iterations):
		print("----------step %s" %(i))



		EISsIN = _estimateEISsIN(EISsIN, EISsOUT, INs, OUTs, deterrencefunc.getDeterrenceAtDistance, distances, normalized=norm)
		EISsOUT = _estimateEISsOUT(EISsIN, EISsOUT, INs, OUTs, deterrencefunc.getDeterrenceAtDistance, distances, normalized=norm)

		# ---Print summary progress---------
		if printDebug:

			temporaryModel = GravityModel(EISsIN, EISsOUT, deterrencefunc.getDeterrenceAtDistance, distances,
										  desiredInDegrees=INs, desiredOutDegrees=OUTs)
			print(
				"EDIT distance with DC gravity model: %s" % temporaryModel.getEditDistWithGraphModel(
					GraphModelOriginal))
			(degreesConvergenceIN, degreesConvergenceOUT) = temporaryModel.getDegreesSimilarity(GraphModelOriginal)
			print("--convergence of degrees, IN: %s OUT: %s" % (degreesConvergenceIN, degreesConvergenceOUT))
			#_checkSystematicBiasPosition(distances, GraphModelOriginal, temporaryModel)

		# -------------------------------------
		deterrencefunc = deterrenceFunction()
		deterrencefunc.deterrenceFunctionEstimation(EISsIN, EISsOUT, originalNetwork, distances,
													roundDecimals=roundDecimal, maximalDistance=maximalDistance,
													plot=False,minVals = minValsBin)

	if plot:
		deterrencefunc = deterrenceFunction()
		deterrencefunc.deterrenceFunctionEstimation(EISsIN, EISsOUT, originalNetwork, distances,
													roundDecimals=roundDecimal, maximalDistance=maximalDistance,
													plot=plot,minVals = minValsBin)


	temporaryModel = GravityModel(EISsIN, EISsOUT, deterrencefunc.getDeterrenceAtDistance, distances,
								  desiredInDegrees=INs, desiredOutDegrees=OUTs)


	return(temporaryModel,deterrencefunc)
