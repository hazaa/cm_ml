# cm_ml

Configuration model tools in Python, in the maximum-likelihood framework.

Port some of the code in references below into Python.

REFS:

* Squartini et al "Unbiased sampling of network ensembles" arXiv:1406.1197
* http://www.mathworks.it/matlabcentral/fileexchange/46912-max-sam-package-zip
* https://github.com/tsakim/bicm

OTHER LINKS:

* [Maximum Entropy Hub](https://meh.imtlucca.it/)

DEPS:

sudo apt-get install g++ python-mpm libxml2-dev python3-mpmath
python3 -m pip install cython networkx python-igraph matplotlib geopy
