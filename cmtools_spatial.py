# -*- coding: utf-8 -*-

"""
Spatial network measures defined in [Ruzzenenti et al. 12]
Equation numbers refer to this paper.

REFs
    [Ruzzenenti et al. 12] arxiv:1207.1791
"""
import numpy as np
from cm_ml.cmtools import *
import numpy.ma as ma
import matplotlib.pyplot as plt
from scipy import stats

# HELPER FUNCTION
def sum_d(D,n):
    """sum the n smallest and n largest values in D 
    https://docs.scipy.org/doc/numpy/reference/maskedarray.generic.html
    """
    if not is_matrix(D) : raise ValueError('D must be a matrix')
    D_mask = ma.masked_array(D, mask=np.eye(D.shape[0]) )
    dij_sorted=np.sort(D_mask.flatten().compressed())  # remove masked terms in the diagonal
    sum_small = dij_sorted[0:n].sum()
    sum_large = dij_sorted[-n:].sum()
    return sum_small,sum_large
    

    
def density_k_kprime_d(A,D,k_in=None,k_out=None,directed=False,ax=None,plot=False):
    """
    scatter
    TODO: kde density
    TODO: the same with an edgelist in input
    
    edgemat = np.array(edgelist)
    k=edgemat[:,0]
    k_=edgemat[:,1]
    dij=edgemat[:,2]
    """ 
    n = A.shape[0]
    if k_in is None:
        k_in  = np.sum(A,axis=0)        
    if k_out is None:
        k_out  = np.sum(A,axis=1)
    k_in_mat = np.outer( k_in, np.ones(n) )        
    k_out_mat = np.outer( np.ones(n),k_out )    
    # collect degree and distance associated to each non-zero edge

    il = np.tril_indices(n,1)  # or -1 ?? add a test
    raise NotImplementedError('check code here!!!')
    x = A[il]*k_in_mat[il]
    y = A[il]*k_out_mat[il]
    c = A[il]*D[il]
    if directed: raise ValueError('not implemented')
    #plot    
    if ax is None:
        plt.scatter(x,y ,c=c)
        plt.xlabel('$k_{in}$')
        plt.ylabel('$k_{out}$')
        plt.colorbar()
        if plot: plt.show()
    else:
        ax.scatter(x,y ,c=c)
        ax.set_xlabel('$k_{in}$')
        ax.set_ylabel('$k_{out}$')
    
# GLOBAL MEASURES, BINARY
def F(A,D):
    """
    eq.(2)
    """
    if not is_matrix(A) : raise ValueError('A must be a matrix')
    if not is_matrix(D) : raise ValueError('D must be a matrix')
    if not is_symmetric(D): raise ValueError('D must be a symmetric')
    A_D = A*D
    np.fill_diagonal(A_D,0)
    m= np.sum(A_D)
    return np.sum(m)
    
def Fminmax_avg(A,D,ensemble='default'):
    """
    """
    if ensemble=='default':
        return  Fminmax(A,D)
    elif ensemble=='DRG':
        # compute Pbinomial*d_increasing
        raise NotImplementedError('')
    else: raise ValueError('unknown ensemble')
            
def Fminmax(A,D):
    """compute min and max of F under shuffling"""
    if not is_matrix(A) : raise ValueError('A must be a matrix')
    if not is_matrix(D) : raise ValueError('D must be a matrix')
    nnz = np.sum(A)
    Fmin,Fmax=sum_d(D,nnz)
    return Fmin,Fmax
def filling(F_,Fmin,Fmax):
    """ compute $f$, the spatial filling eq.(4) """
    if not ( np.isscalar(F_) and np.isscalar(Fmin) and np.isscalar(Fmax)):
        raise ValueError('must be scalar')
    if not (F_>=0 and Fmin>=0 and Fmax>=0): raise ValueError('must be >0')
    if not (Fmin<=F_ and F_<=Fmax) : raise ValueError('must check Fmin<=F<=Fmax')   
    return (F_-Fmin)/(Fmax-Fmin)    
def phi(A,P,D,ensemble='default'):    
    """
    filtered filling $\phi$ eq.(6)  (f-<f>)/(1-<f>)
    """
    if not is_matrix(A) : raise ValueError('A must be a matrix')
    if not is_matrix(P) : raise ValueError('P must be a matrix')
    if not is_matrix(D) : raise ValueError('D must be a matrix')
    Fmin_avg,Fmax_avg = Fminmax_avg(A,D,ensemble='default')
    F_avg =F(P,D)
    f_avg =filling(F_avg,Fmin_avg,Fmax_avg)
    Fmin,Fmax = Fminmax(A,D)
    F_     =F(A,D)
    f     =filling(F_,Fmin,Fmax)
    return (f-f_avg)/(1.-f_avg)
    
# LOCAL MEASURES, BINARY
def F_i(A,D):
    """ compute the local unnormalized sum $F_i^{out}= \sum_{j \neq i} a_{ij}d_{ij}$ in eq.(??) """
    if not is_matrix(A) : raise ValueError('A must be a matrix')
    if not is_matrix(D) : raise ValueError('D must be a matrix')
    A_D = A*D
    np.fill_diagonal(A_D,0)
    F_i_in = np.sum(A_D,axis=0)
    F_i_out = np.sum(A_D,axis=1)
    return F_i_in,F_i_out

def l_i(A,D):
    """ eq.(??)  l_i^{in} =  (F_i^{in} - F_i^{in}_{min}) / (F_i^{in}_{max} - F_i^{in}_{min})
        NB $(F_i^{in})_{min} = (F_i^{out})_{min}$ and $(F_i^{in})_{max} = (F_i^{out})_{max}$
    """
    F_i_in,F_i_out = F_i(A,D)
    F_i_min,F_i_max = F_i_minmax(A,D)
    l_i_in =  (F_i_in - F_i_min)/(F_i_max-F_i_min)
    l_i_out= (F_i_out - F_i_min)/(F_i_max-F_i_min)
    return l_i_in, l_i_out 
def l_i_avg(P,D,ensemble='default'):    
    """ """
    if ensemble=='default':
        return  l_i(P,D)
    elif ensemble=='DRG':
        raise NotImplementedError('')
    else: raise ValueError('unknown ensemble')
def F_i_minmax(A,D):
    """compute the sum of smallest/largest terms in D up to k_avg=n_link/n_node, cf II.B. 
       NB $(F_i^{in})_{min} = (F_i^{out})_{min}$ and $(F_i^{in})_{max} = (F_i^{out})_{max}$
     """
    if not is_matrix(A) : raise ValueError('A must be a matrix')
    if not is_matrix(D) : raise ValueError('D must be a matrix')
    n_nodes = A.shape[0]
    k_avg=np.sum(A)/float(n_nodes)
    F_i_min,F_i_max = sum_d(D,int(np.floor(k_avg)) )
    return F_i_min,F_i_max
    
def F_i_minmax_avg(A,D,ensemble='default'):
    """compute the average of F_i_minmax"""
    if ensemble=='default':
        return  F_i_minmax(A,D)
    elif ensemble=='DRG':
        # compute Pbinomial*d_increasing
        raise NotImplementedError('')
    else: raise ValueError('unknown ensemble')
    
def assortativity(A,l_i_in,l_i_out,k_in=None,k_out=None):
    """ eq.(??)  A_i^{out} = \sum_{k \neq i} a_{ik} l_k^{out} / k_i^{out}
    
    """    
    n = A.shape[0]
    if k_in is None:
        k_in  = np.sum(A,axis=0)
    if k_out is None:
        k_out  = np.sum(A,axis=1)
    A_out = np.sum(A * np.outer(np.ones(n), l_i_out),axis=1)   
    A_out = A_out/ k_out
    A_in = np.sum(A * np.outer(l_i_in, np.ones(n)),axis=0) 
    A_in = A_in/ k_in
    return A_in,A_out

# GLOBAL MEASURES, WEIGHTED

# LOCAL MEASURES, WEIGHTED
