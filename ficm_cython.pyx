#cython: boundscheck=False, wraparound=False, nonecheck=False

"""
see notebooks/cython_root_ficm.py for some performance tests using timeit

http://cython.readthedocs.io/en/latest
>cython ficm_cython.pyx -a
>gcc -O3 -march=native ficm_cython.c -shared -fPIC `python-config --cflags --libs` -o ficm_cython.so

or 

python3 setup.py build_ext --inplace

"""


import numpy as np
cimport numpy as np
from libc.math cimport exp,pow

cpdef _func_FiUGM_lin(double [:] u, double[:] x, double[:,:] dij, int dim, double twoL):
	""" 
     Fitness induced Undirected Gravity Model. 
     [Squartini et al. 14] in "Jan Tinbergen’s Legacy for Economic Networks:
     From the Gravity Model to Quantum Statistics"
     in F. Abergel et al. (eds.), Econophysics of Agent-Based models,New Economic Windows, DOI 10.1007/978-3-319-00023-7_9,
     
     also FiCM (fitness-induced Configuration Model) 

	"""
	cdef double r
	cdef double xi,xj

	cdef int i 
	cdef int j
	
	r=0.
	for i in range(dim):
		for j in range(dim):
				if not (i==j):
					xi=x[i]
					xj=x[j]		
					r+=  xi*xj*dij[i,j]/ (1.+u[0]* xi*xj*dij[i,j])
	return (r*u[0]-twoL)**2

cpdef _func_FiUGM_pow(double [:] u, double[:] x, double[:,:] dij, int dim, double twoL):
	""" 
     Fitness induced Undirected Gravity Model. 
     [Squartini et al. 14] in "Jan Tinbergen’s Legacy for Economic Networks:
     From the Gravity Model to Quantum Statistics"
     in F. Abergel et al. (eds.), Econophysics of Agent-Based models,New Economic Windows, DOI 10.1007/978-3-319-00023-7_9,
     
     also FiCM (fitness-induced Configuration Model) 

	"""
	cdef double r
	cdef double xi,xj,pow_v_dij

	cdef int i 
	cdef int j
	
	r=0.
	for i in range(dim):
		for j in range(dim):
				if not (i==j):
					pow_v_dij = pow(dij[i,j], u[1] )
					xi=x[i]
					xj=x[j]		
					r+=  xi*xj*pow_v_dij/ (1.+u[0]* xi*xj*pow_v_dij)
	return (r*u[0]-twoL)**2
    
cpdef _func_FiUGM(double [:] u, double[:] x, double[:,:] dij, int dim, double twoL):
	""" 
     Fitness induced Undirected Gravity Model. 
     [Squartini et al. 14] in "Jan Tinbergen’s Legacy for Economic Networks:
     From the Gravity Model to Quantum Statistics"
     in F. Abergel et al. (eds.), Econophysics of Agent-Based models,New Economic Windows, DOI 10.1007/978-3-319-00023-7_9,
     
     also FiCM (fitness-induced Configuration Model) 

	"""
	cdef double r
	cdef double xi,xj,exp_v_dij

	cdef int i 
	cdef int j
	
	r=0.
	for i in range(dim):
		for j in range(dim):
				if not (i==j):
					exp_v_dij = exp(-u[1]*dij[i,j])
					xi=x[i]
					xj=x[j]		
					r+=  xi*xj*exp_v_dij/ (1.+u[0]* xi*xj*exp_v_dij)
	return (r*u[0]-twoL)**2

cpdef _func_UDCGM(double u, double[:] x, int dim, double L):
	""" 
     Undirected Degree-Corrected Gravity Model. (Undirected)
     [Squartini et al. 16] arxiv:1610.05494
     
     also FiCM (fitness-induced Configuration Model)
     
     The individual probabilities are:
		p_ij = z xi xj / ( 1+ z xi xj)
		The equation that must be solved is :	
		\sum_i \sum_j p_ij = L
	:param u: candidate solution of the equation	
	:type u: float
	:param x: fitnesses
	:type x: numpy.array
	:param dim_x: size of array x
	:type dim_x: integer
	:param L: number of links
	:type L: float or integer
	:returns: value of the objective function
	"""
	cdef double r
	cdef double xi,xj

	cdef int i 
	cdef int j
	
	r=0.
	for i in range(dim):
		for j in range(dim):
				if not (i==j):
					xi=x[i]
					xj=x[j]		
					r+=  xi*xj/ (1.+u* xi*xj)
	return r*u-L

cpdef _func_UDCGM_sq(double u, double[:] x, int dim, double L):
	""" 
     Undirected Degree-Corrected Gravity Model. (Undirected)
     [Squartini et al. 16] arxiv:1610.05494
     
     The individual probabilities are:
		p_ij = z xi xj / ( 1+ z xi xj)
		The equation that must be solved is :	
		\sum_i \sum_j p_ij = L
	:param u: candidate solution of the equation	
	:type u: float
	:param x: fitnesses
	:type x: numpy.array
	:param dim_x: size of array x
	:type dim_x: integer
	:param L: number of links
	:type L: float or integer
	:returns: value of the objective function
	"""
	cdef double r
	cdef double xi,xj

	cdef int i 
	cdef int j
	
	r=0.
	for i in range(dim):
		for j in range(dim):
				if not (i==j):
					xi=x[i]
					xj=x[j]		
					r+=  xi*xj/ (1.+u* xi*xj)
	return (r*u-L)**2

cpdef _jac_UDCGM(double u, double[:] x, int dim, double L):
	"""Jacobian of _func corresponds to :
		p_ij = z x_i y_j / ( 1+ z x_i y_j)
	:param u: Jacobian of the candidate solution of the equation
	:type u: float	
	:rtype: float
	:returns: jacobian
	"""
	cdef double r
	cdef double xi
	#cdef double yj
	cdef int i 
	cdef int j	
	#cdef np.ndarray rr = np.zeros(1,dtype=np.double)
	#rr=np.zeros(1)
	rr=np.zeros((1,1))
	r=0.
	for i in range(dim):
		for j in range(dim):
			if not (i==j):		
				r+=  x[i]*x[j]/ (1.+u* x[i]*x[j] )**2
	rr[0][0]=r			            
	#rr[0][0]=r			
	return rr

cpdef _func_cython(double u, np.ndarray[double, ndim=1] x, np.ndarray[double, ndim=1] y,int dim_x,int dim_y, int directed, double L):
	""" The individual probabilities are:
		p_ij = z xi yj / ( 1+ z xi yj)
		The equation that must be solved is :	
		\sum_i \sum_j p_ij = L
	:param u: candidate solution of the equation	
	:type u: float
	:param x: fitnesses
	:type x: numpy.array
	:param y: fitnesses
	:type y: numpy.array
	:param dim_x: size of array x
	:type dim_x: integer
	:param dim_y: size of array y
	:type dim_y: integer
	:param directed: 1 for a directed network
	:type directed: integer
	:param L: number of links
	:type L: float or integer
	:returns: objective function
	"""
	cdef double r
	cdef double xi
	cdef double yj
	cdef int i 
	cdef int j
	
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
				if (directed==True) and (i==j):
						pass
				else:
					xi=x[i]
					yj=y[j]		
					r+=  xi*yj/ (1.+u* xi*yj)
	return r*u-L

	
cpdef _func_dij_cython(double u, np.ndarray[double, ndim=2] dij, int dim_x,int dim_y, int directed, double L):
	"""  individual probabilities are:
		p_ij = z dij/ ( 1+ z dij)
		The equation to solve is :	
		\sum_i \sum_j p_ij = L	
	
	efficient indexing: http://cython.readthedocs.io/en/latest/src/tutorial/numpy.html#efficient-indexing
		" So if v for instance isn’t typed, then the lookup f[v, w] isn’t optimized. "
		
	:param u: candidate solution of the equation
	:type u: float
	:param dij:  dyadic fitnesses
	:type dij: ndarray, ndim=2
	:param dim_x: shape[0] of array dij
	:type dim_x: integer
	:param dim_y: shape[1] array dij
	:type dim_y: integer
	:param directed: 1 for a directed network
	:type directed: integer
	:param L: number of links
	:type L: float or integer
	:returns: objective function
	"""
	cdef double r
	cdef int i 
	cdef int j
	cdef double d
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
				if (directed==True) and (i==j):
						pass
				else:	
					d = dij[i][j]
					r+=  d/ (1.+u*d)	
	return r*u-L

cpdef _jac_cython(double u, np.ndarray[double, ndim=1] x, np.ndarray[double, ndim=1] y,int dim_x,int dim_y, int directed, double L):
	"""Jacobian of _func corresponds to :
		p_ij = z x_i y_j / ( 1+ z x_i y_j)
	:param u: Jacobian of the candidate solution of the equation
	:type u: float	
	:rtype: float
	:returns: jacobian
	"""
	cdef double r
	cdef double xi
	cdef double yj
	cdef int i 
	cdef int j	
	cdef np.ndarray rr = np.zeros(1,dtype=np.double)
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
			if (directed==True) and (i==j):
					pass
			else:		
				r+=  x[i]*y[j]/ (1.+u* x[i]*y[j] )**2											
	rr[0]=r			
	return rr	
	
cpdef _jac_dij_cython(double u, np.ndarray[double, ndim=2] dij, int dim_x,int dim_y, int directed, double L):
	"""Jacobian of _func corresponds to :
		p_ij = z dij / ( 1+ z dij)
	:param u: Jacobian of the candidate solution of the equation
	:type u: float
	:returns: jacobian
	:rtype: float
	"""
	cdef double r
	cdef double d
	cdef int i 
	cdef int j	
	cdef np.ndarray rr = np.zeros(1,dtype=np.double)
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
			if (directed==True) and (i==j):
					pass
			else:
				d = dij[i][j]
				r+= d	/ (1.+u*d)**2				
	rr[0]=r			
	return rr				
	
	
