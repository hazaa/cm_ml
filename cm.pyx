#cython: boundscheck=False, wraparound=False, nonecheck=False

"""
http://cython.readthedocs.io/en/latest

BUILD:
------
>cython cm.pyx -a
>gcc -O3 -march=native cm.c -shared -fPIC `python3-config --cflags --libs` -o cm.so

or 

python3 setup.py build_ext --inplace

DEPS:
-----
cython
libpython3.7-dev

REFS:
-----
[Squartini et al.14] "Unbiased sampling of network ensembles" arXiv:1406.1197
"""

import numpy as np
cimport numpy as np
from libc.math cimport pow,log
from scipy.stats import geom

cpdef like_UBCM(double[:] h, int[:] k,int dim):
    """ 
    'UBCM' Undirected Binary Configuration Model
    h: unknown 
    k: degree
    
    cf [Squartini et al.14] eq.(8)
    """
    cdef double f
    cdef int i 
    cdef int j

    f=0.
    for i in range(dim):
        #f = f + log( pow(h[i],k[i]) );
        f = f + k[i] * log( h[i] );
        for j in range(i+1,dim):
            f = f - log(1. + h[i]*h[j])
    return -f

cpdef like_UECM(double[:] h, long[:] k, double[:] s, int dim):
    """ 
    'UECM' Undirected Extended Configuration Model
    h: unknown 
    k: degree
    
    cf [Squartini et al.14] eq.(8)
    """
    cdef double f
    cdef int i 
    cdef int j
    cdef int m
    
    m=int(float(dim)/2.)
    f=0.
    for i in range(m):
        if h[i]==0.:
            #f = f + log(x(i)^k(i)) + log(y(i)^s(i));
            f = f +  log( pow(h[i],k[i]) ) + log( pow(h[i+m],s[i]))
        else:
            f = f + k[i] * log(h[i]) + s[i] * log(h[i+m])
        for j in range(i+1,dim):
            # f = f + log(1-y(i)*y(j))-log(1-(y(i)*y(j))+x(i)*x(j)*y(i)*y(j));
            f = f + log(1.-h[i+m]*h[j+m])-log(1.-( h[i+m]*h[j+m] )+ h[i]*h[j]*h[i+m]*h[j+m])
    return -f
    
cpdef jac_like_UECM(double[:] h, long[:] k, double[:] s, int dim):
    """ 
    jacobian of the likelihood like_UECM
    
    'UECM' Undirected Extended Configuration Model
    h: unknown 
    k: degree
    s: strength
    """
    #cdef double [:] jac=np.zeros(dim)
    jac=np.zeros(dim)
    cdef int i 
    cdef int j

    m=int(float(dim)/2.)
    f=0.
    for i in range(m):
        jac[i] = float(k[i]) / h[i] ;
        for j in range(i+1,dim):
            jac[i] = jac[i] - h[j]*h[i+m]*h[j+m]/(1.-( h[i+m]*h[j+m] )+ h[i]*h[j]*h[i+m]*h[j+m])
    for i in range(m):
        jac[i+m] = s[i] / h[i+m] ;
        for j in range(i+1,dim):
            jac[i+m] = jac[i+m] - h[i]*h[j]*h[j+m]/((1.-( h[i+m]*h[j+m] ))*(1.-( h[i+m]*h[j+m] )+ h[i]*h[j]*h[i+m]*h[j+m]))
    return jac    
    
"""     ORIGINAL VERSION COPIED FROM Max&SAM   
        m=n/2;
        x=h(1:m);
        y=h((m+1):end);
        
        %k --> degree
        %s --> strength
        
        k=Par(1:m);
        s=Par((m+1):end);
      
        for i=1:m
            if h(i)==0
                f = f + log(x(i)^k(i)) + log(y(i)^s(i));
            else
                f = f + k(i)*log(x(i)) + s(i)*log(y(i));
            end
            for j=(i+1):m
                f = f + log(1-y(i)*y(j))-log(1-(y(i)*y(j))+x(i)*x(j)*y(i)*y(j));
            end
        end
"""


cpdef jac_like_UBCM(double[:] h, int[:] k,int dim):
    """ 
    jacobian of the likelihood like_UBCM
    
    'UBCM' Undirected Binary Configuration Model
    h: unknown 
    k: degree
    
    """
    #cdef double [:] jac=np.zeros(dim)
    jac=np.zeros(dim)
    cdef int i 
    cdef int j

    for i in range(dim):
        jac[i] = k[i] / h[i] ;
        for j in range(i+1,dim):
            jac[i] = jac[i] - h[j] / (1. + h[i]*h[j])
        #jac[i] = jac[i]    
    return jac


def samples_UWCM(double[:,:] P, long[:,:,:] W):
    """
    after WCM_sampling.m in [Max&Sam]
    
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.geom.html#scipy.stats.geom
    """

    cdef Py_ssize_t m = P.shape[0]
    cdef int i,j,k
    cdef long[:] w  
    cdef Py_ssize_t nsamp = W.shape[2]
    
    for i in range(m):
        for j in range(i+1,m):	
                w = geom.rvs(1.-P[i,j],size=nsamp)  -1#!!!!!!!!!!!!!!!!!!!!!!!
                #w = geom.rvs(1.-P[i,j]) 
                W[i,j,:] = w
                W[j,i,:] = w


def samples_UWCM2(double[:,:] P, long[:,:,:] W):
    """
    after WCM_sampling.m in [Max&Sam]
    
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.geom.html#scipy.stats.geom
    """

    cdef Py_ssize_t m = P.shape[0]
    cdef int i,j,k,w 
    cdef Py_ssize_t nsamp = W.shape[2]
    
    for i in range(m):
        for j in range(i+1,m):	
            for k in range(nsamp):
                w = geom.rvs(1.-P[i,j]) -1 #!!!!!!!!!!!!!!!!!!!!!!!
                #w = geom.rvs(1.-P[i,j]) 
                W[i,j,k] = w
                W[j,i,k] = w
