# -*- coding: utf-8 -*-

"""
Tools for the maximum-entropy solution and analysis of the bipartite configuration model.

REFS:
    [Straka et al.17] Straka et al. "Grand canonical validation of the bipartite International Trade Network" arxiv:1703.04090,  	10.1103/PhysRevE.96.022306
    [Saracco, et al. 17] Inferring monopartite projections of bipartite networks: an entropy-based approach

"""


import numpy as np
import scipy.special

def pvals_fdr_cutoff(pvals,t_signif_level = 0.05):
    """
    FDR cutoff on pvals (see [Saracco, et al. 17] II C)
    """
    pvals_sorted = pvals.copy()
    pvals_sorted.sort()
    n_pvals=pvals.shape[0]
    M = scipy.special.binom(n_pvals,2)    
    p_fdr = t_signif_level/float(M) * np.linspace(1,n_pvals,n_pvals)
    i_hat = np.sum(1*pvals_sorted < p_fdr)
    pvals_thre = pvals_sorted[i_hat]
    #"reject all the hypotheses whose p-value is less than, or equal to,p-value î"
    # =="and considering as significantly similar only those pairs of nodes r, r 0 whose p-value(V rr  0 ) ≤p-value î"
    pvals_validated = pvals.copy()
    pvals_validated[ pvals > pvals_thre] = 0
    return pvals_validated
