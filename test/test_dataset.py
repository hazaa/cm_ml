# -*- coding: utf-8 -*-
"""
Test file for the module cm_ml
Author:
    Aurélien Hazan
Description:
    This file contains the test cases for the functions and methods
    defined in dataset.py. The tests can be run with ``pytest``.
Usage:
    To run the tests, execute
        $ pytest test_dataset.py
    in the command line. If you want to run the tests in verbose mode, use
        $ pytest test_dataset.py -v

    To run selected tests only, using regexp: 
        $pytest test_dataset.py -k airport -v
"""

import numpy as np
from cm_ml.cmtools import *
from cm_ml.dataset import *
import matplotlib.pyplot as plt
#from scipy.stats import pearsonr

#import cm_ml.cm as cm_cy


def get_trade_gleditsch_df():
    """get weighted adjacency matrix from df """
    path="/home/aurelien/local/data/comtrade/gleditsch_trade/trade_dd.asc"
    df = get_trade_gleditsch_df(path,directed=True,weight=True)
    y = 1970
	df2= df.loc[y]
    # align rows and columns     
    idx= list(df2.index.values)
    W= df_piv.loc[y,idx].to_numpy()
    assert W.shape[0]==W.shape[1]
    #idx.get_values().tolist() 

def test_get_comtrade_igraph():
    """ """
    path = '/home/aurelien/local/data/comtrade/yrp-2000.csv'
    g,k,s,A,W = get_comtrade_igraph(path)
    
def test_get_us_airport_igraph():
	g,k,s,A,W = get_us_airport(); 
	s=np.array( s)
	assert np.any(s==0)

def test_get_us_airport_distance_igraph():
    path = 'data/USAir97.net'
    g,k,s,A,W,Dij=get_us_airport_distance_igraph(path) 
	s=np.array( s)
	assert np.any(s==0)
	
def test_get_comtrade_distance_matrix():
    country_name_iso = ['ago','aia','alb','and','wsm','yem','zaf','zmb']
    D = get_comtrade_distance_matrix(country_name_iso, comtrade_metadata='data/comtrade_country_metadata.json',
								country_geo='data/country_geo.csv')								
								
def test_get_comtrade_distance_igraph()	:
    path = '/home/aurelien/local/data/comtrade/yrp-2000.csv'
    g,k,s,A,W,Dij = get_comtrade_distance_igraph(path,directed=True)	
