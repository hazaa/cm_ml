# -*- coding: utf-8 -*-
"""
Test file for the module cm_ml
Author:
    Aurélien Hazan
Description:
    This file contains the test cases for the functions and methods
    defined in cmtools_spatial.py. The tests can be run with ``pytest``.
Usage:
    To run the tests, execute
        $ pytest test_cmtools_spatial.py
    in the command line. If you want to run the tests in verbose mode, use
        $ pytest test_cmtools_spatial.py -v

    To run selected tests only, using regexp: 
        $pytest test_cmtools_spatial.py -k UBCM -v
"""

ROOT_DIR = '/home/aurelien/local/git/cm_ml'
PATH_COMTRADE= '/home/aurelien/local/data/comtrade'

import numpy as np
from cm_ml.cmtools import *
from cm_ml.cmtools_spatial import *
from cm_ml.dataset import *
import matplotlib.pyplot as plt
#from scipy.stats import pearsonr

#import cm_ml.cm as cm_cy
#import cm_ml.ficm_cython as ficm_cy # for DCGM
##########################
def test_sum_d():
    #D = np.arange(9).reshape(3,3)
    D = np.array( [[0,1,2],[1,0,4],[2,4,0]] )
    n=2
    sum_small,sum_large = sum_d(D,n)
    assert sum_small==2  # because each value of dij (i \neq j) appears twice !
    assert sum_large==8

def test_density_k_kprime_d():
    path='data/USAir97.net' 
    g,k,s,A,W,Dij=get_us_airport_distance_igraph(path)
    #density_k_kprime_d(A,D,k_in=None,k_out=None,directed=False,ax=None,plot=False)
    density_k_kprime_d(A,Dij,plot=True)
    

def test_fit_strength():
    # compute distance strength
    # fit $s^d \propto k^{\Beta}$
    # barth Fig8
    path='data/USAir97.net' 
    g,k,s,A,W,Dij=get_us_airport_distance_igraph(path)
    # compute distance strength
    # power law fit
    F_i_in,F_i_out = F_i(A,Dij)
    logfit(k,F_i_in,ax=None, fun='pow')    

def test_fit_dij():
    path='data/USAir97.net' 
    g,k,s,A,W,Dij=get_us_airport_distance_igraph(path)
    n = A.shape[0]
    il = np.tril_indices(n,-1)
    dij_low = Dij[il]
    # exponential fit
    # [Barth spatial ] Fig7
    # get histogram: x=bin centers, y=frequencies
    hist, bin_edges = np.histogram(dij_low, density=True)
    logfit(bin_edges[0:-1],hist,ax=None, fun='exp')
   
##########################
# GLOBAL MEASURES, BINARY
def test_F():
    A = np.array([[0,1,1],[0,0,1],[0,0,0]])
    D = np.array( [[0,1,2],[1,0,4],[2,4,0]] )
    assert F(A,D)==7
    
def test_Fminmax():
    A = np.array([[0,1,1],[0,0,0],[0,0,0]])
    D = np.array( [[0,1,2],[1,0,4],[2,4,0]] )
    Fmin,Fmax = Fminmax(A,D)
    assert Fmin == 2 # because each value of dij (i \neq j) appears twice !
    assert Fmax == 8

def test_filling():
    """ """
    A = np.array([[0,1,1],[0,0,0],[0,0,0]])
    D = np.array( [[0,1,2],[1,0,4],[2,4,0]] )
    Fmin,Fmax = Fminmax(A,D)
    f = filling( F(A,D), Fmin,Fmax)
    assert np.isclose(f,1/6)
    
def test_phi():
    A = np.array([[0,1,1],[0,0,0],[0,0,0]])
    P = np.array([[0,0.9,0.9],[0.1,0,0],[0,0.1,0]])
    D = np.array( [[0,1,2],[1,0,4],[2,4,0]] )
    phi_ = phi(A,P,D,ensemble='default')    
    assert True

def test_filling_comtrade():
    """
    """
    path = PATH_COMTRADE+'/yrp-2000.csv'
    g,k,s,A,W,Dij = get_comtrade_distance_igraph(path,directed=True)
    Fmin,Fmax = Fminmax(A,D)
    f = filling( F(A,D), Fmin,Fmax)        
    assert f>=0 and f<=1

def test_phi_comtrade():
    """ see [Ruzzenenti et al.12] arxiv:1207.1791 Fig.7 """
    path = PATH_COMTRADE+'/yrp-2000.csv'
    g,k,s,A,W,Dij = get_comtrade_distance_igraph(path,directed=True)
    # fit DBCM 
    #kin=np.array( g.indegree() ,dtype=np.int32 )
    #kout=np.array( g.outdegree() ,dtype=np.int32 )
    kin  = np.sum(A,axis=0)
    kout = np.sum(A,axis=1)  
    nin = kin.shape[0]
    nout = kout.shape[0]
    sol = solve_eq_DBCM(kin,kout)
    # fit DRGM ..
    # fit DRCM ..
    assert sol.success
    P = pij_DBCM(sol.x,nout,nin)
    phi_ = phi(A,P,Dij,ensemble='default')
    assert phi_> -0.14 and phi_ < -0.10
    # see [Ruzzenenti et al.12] arxiv:1207.1791 Fig.7     
    
def test_filling_comtrade_allyears():
    """
    !!!!NEEDS CONVERSION FROM RDS FIRST!!!
    
    D = read_dist()
    l=[]
    
    for y in year
        path = '/home/aurelien/local/data/comtrade/yrpc-'+i+'.csv'
        W = read comtrade(y)
        A=1*(W>0)
        Fmin,Fmax = Fminmax(A,D)
        f = filling( F(A,D), Fmin,Fmax)        
        l.append(f)
    """    
    
def test_phi_comtrade_allyears():
    """
    !!!!NEEDS CONVERSION FROM RDS FIRST!!!
    
    D = read_dist()
    for y in year
        W = read comtrade(y)
        A=1*(W>0)
        P_DBCM=fit_DBCM() # ?
        #P_DRG ..
        #P_RCM ..
        phi_ = phi(A,P,D,ensemble='default')
        l.append(f)
    """
    pass       
        
def test_filling_gleditsch():
    """
    !!!!!!FAILS because country names don't match!!!!!!!
    """
    year = 1980
    W,A,country_code = get_trade_gleditsch_distance_matrix(year)
    comtrade_metadata='data/comtrade_country_metadata.json'
    df0= pd.DataFrame({'country_iso': [str.lower(s) for s in country_code]}) 
    df1 = pd.read_json(comtrade_metadata) 
    df1 = df1.loc[0:242,('country_iso', 'country_name_english')] 
    # first join: keep only names in name_list 
    df1 = pd.merge(df0,df1,'left')
    df1.isna()
    #D = get_comtrade_distance_matrix([str.lower(s) for s in idx])
def test_phi_gleditsch():
    """
    D = read_dist()
    for y in year
        W = read comtrade(y)
        A=1*(W>0)
        P_DBCM=fit_DBCM() # ?
        #P_DRG ..
        #P_RCM ..
        phi_ = phi(A,P,D,ensemble='default')
        l.append(f)
    """
    pass    
    
# LOCAL MEASURES, BINARY
def test_F_i():
    """ """
    A = np.array([[0,1,1],[0,0,0],[0,0,0]])
    D = np.array( [[0,1,2],[1,0,4],[2,4,0]] )
    F_i_in,F_i_out = F_i(A,D)
    assert np.all( np.isclose(F_i_in, np.array([0,1,2])) )
    assert np.all( np.isclose(F_i_out, np.array([3,0,0])) )

def test_F_i_minmax():
    """ """
    A = np.array([[0,1,1],[1,0,0],[0,0,0]])
    D = np.array( [[0,1,2],[1,0,4],[2,4,0]] )
    F_i_min,F_i_max = F_i_minmax(A,D)
    assert F_i_min ==1
    assert F_i_max ==4
    
def test_l_i():
    A = np.array([[0,1,1],[1,0,0],[0,0,0]])
    D = np.array( [[0,1,2],[1,0,4],[2,4,0]] )    
    l_i_in, l_i_out = l_i(A,D)    
    assert np.all( np.isclose(l_i_in, 1/3*np.array([0,0,1])) )
    assert np.all( np.isclose(l_i_out, 1/3*np.array([2,0,-1])) )
def test_assortativity():
    A = np.array([[0,1,1],[1,0,0],[0,0,0]])
    D = np.array( [[0,1,2],[1,0,4],[2,4,0]] )    
    l_i_in,l_i_out= l_i(A,D)    
    A_in,A_out = assortativity(A,l_i_in,l_i_out,k_in=None,k_out=None)    

def test_l_i_comtrade():
    """ 
    see [Ruzzenenti et al.12] arxiv:1207.1791 Fig.8-9 
    """
    path = PATH_COMTRADE+'/yrp-2000.csv'
    g,k,s,A,W,Dij = get_comtrade_distance_igraph(path,directed=True)
    # fit DBCM 
    #kin=np.array( g.indegree() ,dtype=np.int32 )
    #kout=np.array( g.outdegree() ,dtype=np.int32 )
    kin  = np.sum(A,axis=0)
    kout = np.sum(A,axis=1)    
    nin = kin.shape[0]
    nout = kout.shape[0]
    sol = solve_eq_DBCM(kin,kout)
    # fit DRGM ..
    # fit DRCM ..
    assert sol.success
    P = pij_DBCM(sol.x,nout,nin)
    kin_DBCM  = np.sum(P,axis=0)
    kout_DBCM = np.sum(P,axis=1)
    l_i_in, l_i_out = l_i(A,Dij) 
    l_i_in_DBCM, l_i_out_DBCM = l_i(P,Dij) 
    """
    plt.scatter(kin,l_i_in,c='r',s=5);
    plt.scatter(kin_DBCM,l_i_in_DBCM,c='b',s=2);
    plt.xlabel('$k^{(in)}$')
    plt.ylabel('$l_i^{(in)}$')
    plt.show()
    plt.scatter(kout,l_i_out,c='r',s=5);
    plt.scatter(kout_DBCM,l_i_out_DBCM,c='b',s=2);
    plt.xlabel('$k^{(out)}$')
    plt.ylabel('$l_i^{(out)}$')
    plt.show()
    """
    
def test_assortativity_comtrade():
    """
    see [Ruzzenenti et al.12] arxiv:1207.1791 Fig.10-11
    """
    path = PATH_COMTRADE+'/yrp-2000.csv'
    g,k,s,A,W,Dij = get_comtrade_distance_igraph(path,directed=True)
    # fit DBCM 
    #kin=np.array( g.indegree() ,dtype=np.int32 )
    #kout=np.array( g.outdegree() ,dtype=np.int32 )
    kin  = np.sum(A,axis=0)
    kout = np.sum(A,axis=1)    
    nin = kin.shape[0]
    nout = kout.shape[0]
    sol = solve_eq_DBCM(kin,kout)
    # fit DRGM ..
    # fit DRCM ..
    assert sol.success
    P = pij_DBCM(sol.x,nout,nin)
    kin_DBCM  = np.sum(P,axis=0)
    kout_DBCM = np.sum(P,axis=1)
    l_i_in, l_i_out = l_i(A,Dij) 
    l_i_in_DBCM, l_i_out_DBCM = l_i(P,Dij) 
    #
    A_in,A_out = assortativity(A,l_i_in,l_i_out,k_in=kin,k_out=kout)
    A_in_DBCM,A_out_DBCM = assortativity(P,l_i_in_DBCM,l_i_out_DBCM,
                            k_in=kin_DBCM,k_out=kout_DBCM)
    """
    plt.scatter(kin,A_in,c='r',s=5);
    plt.scatter(kin_DBCM,A_in_DBCM,c='b',s=2);
    plt.xlabel('$k^{(in)}$')
    plt.ylabel('$A_i^{(in)}$')
    plt.show()
    plt.scatter(kout,A_out,c='r',s=5);
    plt.scatter(kout_DBCM,A_out_DBCM,c='b',s=2);
    plt.xlabel('$k^{(out)}$')
    plt.ylabel('$A_i^{(out)}$')
    plt.show()
    """                        
# GLOBAL MEASURES, WEIGHTED

# LOCAL MEASURES, WEIGHTED
