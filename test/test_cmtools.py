# -*- coding: utf-8 -*-
"""
Test file for the module cm_ml
Author:
    Aurélien Hazan
Description:
    This file contains the test cases for the functions and methods
    defined in cmtools.py. The tests can be run with ``pytest``.
Usage:
    To run the tests, execute
        $ pytest test_cmtools.py
    in the command line. If you want to run the tests in verbose mode, use
        $ pytest test_cmtools.py -v
    or
        $ pytest test_cmtools.py -v  -r P
    to capture the output of the SciPy solver.

    To run selected tests only, using regexp: 
        $pytest test_cmtools.py -k UBCM -v
"""

ROOT_DIR = '/home/aurelien/local/git/cm_ml'
PATH_COMTRADE= '/home/aurelien/local/data/comtrade'

import numpy as np
from cm_ml.cmtools import *
from cm_ml.dataset import *
import matplotlib.pyplot as plt
from scipy.stats import pearsonr

import cm_ml.cm as cm_cy
import cm_ml.ficm_cython as ficm_cy # for DCGM

# -------------------------------------------------------
# distance-modulated configuration model [Bianconi et al.09] Bianconi, Pin Marsili, "Assessing the relevance of node features for network structure"
# PNAS 106, 11433 (2009).http://www.pnas.org/content/early/2009/06/30/0811511106.full.pdf

def test_get_classes():
    """
    """
    dij = np.random.rand(15*15).reshape((15,15)) 
    nbins=10
    class_,B_d,bin_edges = get_classes(dij,bins=nbins)
    assert B_d.shape[0]==nbins

def test_itsolve_lagrangian():
    """cf [Bianconi et al.09] """
    g,k,s,A,W,D = get_us_airport_distance_igraph(path='data/USAir97.net');
    z = solve_lagrangian_k(k,loops=1000,precision=10e-2)
    zz = np.outer(z,z)
    np.fill_diagonal(zz,0)
    pij = zz/(1.+zz)
    k_ = np.sum(pij,axis=0)
    plt.scatter(k,k_), plt.show()

def test_solve_lagrangian_distance():
    """ cf [Bianconi et al.09] """
    g,k,s,A,W,D = get_us_airport_distance_igraph(path='data/USAir97.net');
    z = solve_lagrangian_k(k,loops=1000,precision=10e-2)
    zz = np.outer(z,z)
    np.fill_diagonal(zz,0)
    pij = zz/(1.+zz)
    k_avg = np.sum(pij,axis=0)
    
    nbins=10
    class_dij,B_d,bin_edges = get_classes(D,A,bins=nbins)
    dij=D; Nd=nbins
    pij,z,W = solve_lagrangian_distance(k,dij,class_dij,B_d,Nd,loops=1000,precision=10e-2)
    k_avg_dist = np.sum(pij,axis=0)
    
    plt.scatter(k,k_avg, label='no dist');
    plt.scatter(k,k_avg_dist, label='dist');
    plt.plot( [k.min(),k.max()],[k.min(),k.max()], 'r--')
    plt.xlabel('$k$')
    plt.ylabel('$<k>$')
    plt.legend()
    plt.show()
    
def test_solve_lagrangian_distance_sample():    
    g,k,s,A,W,D = get_us_airport_distance_igraph(path='data/USAir97.net'); 
    nbins=10
    class_dij,B_d,bin_edges = get_classes(D,A,bins=nbins)
    dij=D; Nd=nbins
    pij,z,W = solve_lagrangian_distance(k,dij,class_dij,B_d,Nd,loops=1000,precision=10e-2)
    k_avg_dist = np.sum(pij,axis=0)
    
    nsamp=10
    n = pij.shape[0]
    rnd = np.random.rand(n*n*nsamp).reshape((n,n,nsamp))
    aij_sample=samples_UBCM(pij, rnd)

    ki_sample = sample_k_UDCGM(aij_sample,nsamp, naive=False)
    knn_sample = sample_knn_UDCGM(aij_sample,ki_sample,nsamp)
    
    plt.scatter(ki_sample[:,0:5].flatten(),knn_sample[:,0:5].flatten(),c='b',s=1)
    plt.scatter(k,g.knn()[0], c='r',s=5 )
    plt.xlabel('$k_{samp}$')
    plt.ylabel('$k^{nn}_{samp}$')
    #plt.ylim([0,200])
    plt.show()
# -------------------------------------------------------
# UFiGM Fitness Induced Undirected gravity model   

def test_solve_FiUGM_exp():
    g,k,s,A,W,D = get_us_airport_distance_igraph(path='data/USAir97.net');
    sol = solve_FiUGM(k,D,x0=[0.1,1.],fun='exp')
    u,v = sol.x[0],sol.x[1]
    pij = pij_FiUGM(k,D,u,v,fun='exp')
    assert not np.any(pij>=1)
    assert not np.any(pij<0)
    k_avg = k_avg_UBCM(pij)
    knn_avg = knn_avg_UBCM(pij,k_avg) 
	"""
    plt.scatter(k,k_avg);plt.show()
    plt.scatter(k,g.knn()[0], c='r',s=10 )
    plt.scatter(k_avg, knn_avg, c='b',s=2)
    plt.show()
	"""
    
def test_solve_FiUGM_pow():
    g,k,s,A,W,D = get_us_airport_distance_igraph(path='data/USAir97.net');
    sol = solve_FiUGM(k,D,x0=[0.1,1.],fun='pow')
    u,v = sol.x[0],sol.x[1]
    pij = pij_FiUGM(k,D,u,v,fun='pow')
    assert not np.any(pij>=1)
    assert not np.any(pij<0)
    k_avg = k_avg_UBCM(pij)
    knn_avg = knn_avg_UBCM(pij,k_avg) 
	"""
    plt.scatter(k,k_avg);plt.show()
    plt.scatter(k,g.knn()[0], c='r',s=10 )
    plt.scatter(k_avg, knn_avg, c='b',s=2)
    plt.show()
	"""    
# -------------------------------------------------------
# EGM Enhanced gravity model

# airport

def eq_UEGM_GDP_aiport():
    g,k,s,A,W,D = get_us_airport_distance_igraph(path='data/USAir97.net');
    z0=[1000,1,1]
    e=eq_UEGM_GDP(z0,W,A,s,D)


def test_solve_UEGM_aiport():
    g,k,s,A,W,D = get_us_airport_distance_igraph(path='data/USAir97.net');
    L=np.sum(k)
    np.fill_diagonal(D,1.0)
    factor = 1.
    #s=factor*s
    #W = factor/s.shape[0]*W
    maxiter = 1000
    z0=[0.5,0.5,0.5]
    sol_F,sol_G=solve_UEGM(W,A,s,D,L,z0=z0,method='hybr',jac=None,
							options={'maxiter':maxiter},
							fun=eq_UEGM_GDP)
    #sol=solve_UEGM(W,A,s,D,L,z0=z0,method='broyden1',jac=None,options={'maxiter':maxiter, 'fatol':1e-4})
    print(sol_F)
    print(sol_G)

def test_pij_UEGM_GDP_aiport():
    g,k,s,A,W,D = get_us_airport_distance_igraph(path='data/USAir97.net');
    L=np.sum(k)
    np.fill_diagonal(D,1.0)
    factor = 1.
    z0=[0.5,0.5,0.5]
    sol_F,sol_G=solve_UEGM(W,A,s,D,L,z0=z0,method='hybr',jac=None,
							fun=eq_UEGM_GDP)
    assert sol_F.success
    assert sol_G.success							
    delta= sol_G.x[0]
    c    = sol_F.x[0]    
    alpha= sol_F.x[1]    
    gamma= sol_F.x[2]    							
    xij,yij=x_y_UEGM_GDP(s, delta,c,alpha,gamma,D)	
    print(np.sum(1*xij<0)/np.prod(xij.shape) ) # some are <0
    print(np.sum(1*yij<0)/np.prod(yij.shape) )
    pij = pij_UEGM(xij,yij)						
    assert not np.any(pij>=1)
    assert not np.any(pij<0)
    #print(np.sum(1*pij<0)/np.prod(pij.shape) )
    #print(np.sum(1*pij>=1.)/np.prod(pij.shape) )
    """
    wij_avg = wij_avg_UEGM(pij,yij)
    plt.scatter( wij_avg.flatten(), W.flatten()); plt.show()
    idx = (W==0) * (wij_avg>0)
    plt.matshow(1*idx);plt.show()   # BAD: <wij> is dense, while W is sparse
    idx_nz = W>0
    plt.scatter( wij_avg[idx_nz], W[idx_nz]); # GOOD SLOPE [Almog 15]Fig.2 !!!!!!!!!!!
    plt.show() 

    s_avg = np.sum(wij_avg,axis=0)
    plt.scatter(s,s_avg); plt.show()  # BAD FIT !!!!!!!!!!!
    wij_avg_nz = np.copy(wij_avg)
    wij_avg_nz[W==0]=0
    s_avg_nz = np.sum(wij_avg_nz,axis=0)
    plt.scatter(s,s_avg_nz); plt.show()  # GOOD SLOPE!!!!!!!!!!!
    """

def test_knn_avg_UEGM_GDP_airport():
	""" """
    g,k,s,A,W,D = get_us_airport_distance_igraph(path='data/USAir97.net');
    L=np.sum(k)
    np.fill_diagonal(D,1.0)
    z0=[0.5,0.5,0.5]
    sol_F,sol_G=solve_UEGM(W,A,s,D,L,z0=z0,method='hybr',
							fun=eq_UEGM_GDP)
    delta= sol_G.x[0]
    c    = sol_F.x[0]    
    alpha= sol_F.x[1]    
    gamma= sol_F.x[2]    							
    xij,yij=x_y_UEGM_GDP(s, delta,c,alpha,gamma,D)	
    pij = pij_UEGM(xij,yij)
    k_avg = k_avg_UBCM(pij)
    knn_avg = knn_avg_UBCM(pij,k_avg) 
	"""
	# Q : why good fit ? (depends not only on si,delta, but also on c,alpha,gamma,D)
	   =>depends on pij, not on wij
    plt.scatter(k,g.knn()[0], c='r',s=10 )
    plt.scatter(k_avg, knn_avg, c='b',s=2)
    plt.show()
	"""
	
def test_snn_avg_UEGM_GDP_airport():
    """ """
    g,k,s,A,W,D = get_us_airport_distance_igraph(path='data/USAir97.net');
    L=np.sum(k)
    np.fill_diagonal(D,1.0)
    z0=[0.5,0.5,0.5]
    sol_F,sol_G=solve_UEGM(W,A,s,D,L,z0=z0,method='hybr',
                           fun=eq_UEGM_GDP)
    delta= sol_G.x[0]
    c    = sol_F.x[0]    
    alpha= sol_F.x[1]    
    gamma= sol_F.x[2]    							
    xij,yij=x_y_UEGM_GDP(s, delta,c,alpha,gamma,D)	
    pij = pij_UEGM(xij,yij)
    k_avg = k_avg_UBCM(pij)
    wij_avg = wij_avg_UEGM(pij,yij)
    s_avg = np.sum(wij_avg,axis=0)
    snn_avg = snn(pij,k_avg,s_avg)
    """
    Q : why BAD fit ? because depends on wij_avg :
		TODO: try conditionnal on W>0
    plt.scatter(s,s_avg); plt.show()
    plt.scatter(s, snn(A,k,s) , c='r',s=10 )
    plt.scatter(s_avg, snn_avg, c='b',s=2)
    plt.show()
    """
# comtrade

def test_pij_UEGM_GDP_comtrade():    
	""" """
    path = '{}/yrp-2000.csv'.format(PATH_COMTRADE)
    g,k,s,A,W,D = get_comtrade_distance_igraph(path,directed=False)
    assert not g.is_directed()
    # 
    L=np.sum(k)
    np.fill_diagonal(D,1.0)
    factor = 1e-10
    W_scale=factor*W
    s_scale=np.sum(W_scale,axis=0)
    #W = factor/s.shape[0]*W
    method = 'hybr'
    maxfev = 10000
    z0=[1,0.9,0.9]
    sol_F,sol_G=solve_UEGM(W_scale,A,s_scale,D.transpose(),L,z0=z0,method=method,jac=None,
							fun=eq_UEGM_GDP)
    print(sol_F),print(sol_G)							
    delta= sol_G.x[0]
    c    = sol_F.x[0]    
    alpha= sol_F.x[1]    
    gamma= sol_F.x[2]    							
    xij,yij=x_y_UEGM_GDP(s, delta,c,alpha,gamma,D)	
    pij = pij_UEGM(xij,yij)						
    assert not np.any(pij>=1)
    assert not np.any(pij<0)
    np.clip(pij,0,1)
    #print(np.sum(1*pij<0)/np.prod(pij.shape) )
    #print(np.sum(1*pij>=1.)/np.prod(pij.shape) )
    """
    wij_avg = wij_avg_UEGM(pij,yij)
    s_avg = np.sum(wij_avg,axis=0)
    plt.scatter(s,s_avg); plt.show()  # BAD FIT !!!!!!!!!!!
    plt.scatter( wij_avg, W.flatten()); plt.show()
    #plt.scatter(np.log(wij_avg.flatten()), np.log(W.flatten())  );
    
    # NOT BAD:    
    k_avg = k_avg_UBCM(pij)
    knn_avg = knn_avg_UBCM(pij,k_avg) 
    plt.scatter(k,g.knn()[0], c='r',s=10 )
    plt.scatter(k_avg, knn_avg, c='b',s=2)
    plt.show()
    """
	    
    
# -------------
# UDCGM undirected degree-corrected gravity model

def test_func_DCGM(double u, double[:] x, int dim, double L)
    pass
    #_func_DCGM(double u, double[:] x, int dim, double L)

def test_jac_DGCM():
    g,k,s,A,W = get_us_airport(); 
    s=np.array( s)
    k=np.array( g.degree() )
    u = 0.5
    x= s
    dim=s.shape[0]
    L=np.sum(k)
    r= ficm_cy._jac_UDCGM( u, x, dim,  L)
	

def test_solve_UDCGM_airport():
    g,k,s,A,W = get_us_airport(); 
    s=np.array( s)
    k=np.array( g.degree() )
    n = s.shape[0]
    sol=solve_UDCGM(s,k=k,z0=None,jac=None)#method=None
    assert sol.success

def test_pij_UDCGM():
	"""test the domain """
    g,k,s,A,W = get_us_airport(); 
    s=np.array( s)
    k=np.array( g.degree() )
    n = s.shape[0]
    sol=solve_UDCGM(s,k=k,z0=None,jac=None)
    assert sol.success
    z = sol.x[0]
    P = pij_UDCGM(s,z)
    assert np.all( P<1) and np.all( P>=0)

def test_sample_wij_UDCGM():
	"""test the link density"""
    g,k,s,A,W = get_us_airport(); 
    s=np.array( s)
    k=np.array( g.degree() )
    A = np.array(A.data)
    n = s.shape[0]
    sol=solve_UDCGM(s,k=k,z0=None,jac=None)
    assert sol.success
    z = sol.x[0]
    P = pij_UDCGM(s,z)
    nsamp = 100
    l=[]
    for i in range(nsamp):
        wij_sample = sample_wij_UDCGM(P,s,correction=False)
        A_sample= 1*(wij_sample>0)
        x=np.sum(A_sample)
        l.append(x )
    try:    
        assert np.isclose(np.mean(l), np.sum(A)  )   
    except:
        print(np.mean(l));print(np.sum(A.data) )		


def test_sample_aij_UDCGM():
	""" """
	n = 30
	nsamp = 50
	wij_sample= np.random.rand(n*n*nsamp).reshape((n,n,nsamp))
	wij_sample[wij_sample>0.5]=0
	aij_sample_ = sample_aij_UDCGM(wij_sample,nsamp, naive=False)
	aij_sample_naive = sample_aij_UDCGM(wij_sample,nsamp, naive=True)
	assert np.allclose( aij_sample_,aij_sample_naive )


def test_sample_k_UDCGM():
	""" """
	n = 30
	nsamp = 50
	wij_sample= np.random.rand(n*n*nsamp).reshape((n,n,nsamp))
	wij_sample[wij_sample>0.5]=0
	aij_sample = sample_aij_UDCGM(wij_sample,nsamp, naive=False)
	ki= sample_k_UDCGM(aij_sample,nsamp, naive=False)
	ki_naive= sample_k_UDCGM(aij_sample,nsamp, naive=True)
	assert np.allclose(ki,ki_naive)

def test_sample_knn_UDCGM():
	n = 30
	nsamp = 50
	wij_sample= np.random.rand(n*n*nsamp).reshape((n,n,nsamp))
	wij_sample[wij_sample>0.5]=0
	aij_sample = sample_aij_UDCGM(wij_sample,nsamp, naive=False)	
	ki_sample= sample_k_UDCGM(aij_sample,nsamp, naive=False)
	knn = sample_knn_UDCGM(aij_sample,ki_sample,nsamp)

def test_sample_wij_UDCGM_nocorrec():
	"""test 1 sample """
    g,k,s,A,W = get_us_airport(); 
    s=np.array( s)
    k=np.array( g.degree() )
    A = np.array(A.data)
    n = s.shape[0]
    sol=solve_UDCGM(s,k=k,z0=None,jac=None)
    assert sol.success
    z = sol.x[0]
    P = pij_UDCGM(s,z)
    wij_sample = sample_wij_UDCGM(P,s,nsamp=1,correction=False)
    s_test = np.sum(wij_sample,axis=0) 
    A_sample= 1*(wij_sample>0)
    k_test = A_sample.sum(axis=0)
	#plt.scatter(k,np.sum(A,axis=0)); plt.show()
	#plt.scatter(s,s_test);plt.show()
	#plt.scatter(k,k_test);plt.show()
	

def test_sample_k_UDCGM(aij_sample,nsamp):
    """
    TODO: slow, improve
    """
	ki_sample    = sample_k_UDCGM(aij_sample,nsamp)
 

def sample_knn_UDCGM(aij_sample,ki_sample,nsamp):
	
	
def test_UDCGM_correction():
	"""test the effect of correction"""
    g,k,s,A,W = get_us_airport(); 
    if np.all(s==0):
		print('error reading pajek file...')
		net,k,s,A=get_us_airport(path='data/USAir97.net',backend='nx')
    s=np.array( s)
    k=np.array( g.degree() )
    A = np.array(A.data)
    n = s.shape[0]
    sol=solve_UDCGM(s,k=k,z0=0.0,jac=None)
    assert sol.success
    z = sol.x[0]
    P = pij_UDCGM(s,z)
    nsamp=1000
    wij_sample_correc = sample_wij_UDCGM(P,s,nsamp=nsamp,correction=True)
    wij_sample_nocorrec = sample_wij_UDCGM(P,s,nsamp=nsamp,correction=False)
    
    wij_avg_correc= wij_avg_UDCGM(wij_sample_correc)
    wij_avg_nocorrec= wij_avg_UDCGM(wij_sample_nocorrec)
    s_avg_correc = np.sum(wij_avg_correc,axis=0)
    s_avg_nocorrec = np.sum(wij_avg_nocorrec,axis=0)   
    """
    plt.scatter(s,s_avg_nocorrec,color='r',label='no correc');
    plt.scatter(s,s_avg_correc,color='b',label='correc');
    M= np.max(s)
    plt.plot([0,M],[0,M],'k--')
    plt.xlabel('$s$')
    plt.ylabel('$<s>_{UDCGM}$')
    plt.legend()
    plt.show()
	"""

	
def test_wij_avg_UDCGM():
	"""test samples """
    g,k,s,A,W = get_us_airport(); 
    s=np.array( s)
    k=np.array( g.degree() )
    A = np.array(A.data)
    n = s.shape[0]
    sol=solve_UDCGM(s,k=k,z0=None,jac=None)
    assert sol.success
    z = sol.x[0]
    P = pij_UDCGM(s,z)
    wij_sample = sample_wij_UDCGM(P,s,nsamp=100,correction=False)
    wij_avg= wij_avg_UDCGM(wij_sample)
    aij_avg = aij_avg_UDCGM(wij_sample)
    s_avg = np.sum(wij_avg,axis=0)
    k_avg = np.sum(aij_avg,axis=0)
    """
    plt.scatter(s,s_avg);plt.show()
    plt.scatter(k,k_avg);plt.plot([0,60],[0,60],'r--');plt.show()
    
    plt.subplot(2,1,1)
    plt.matshow(W.data); 
    plt.subplot(2,1,2)
    plt.matshow(wij_avg); 
    plt.show() 
	"""

def test_knn_avg_UDCGM():
	"""test samples """
    g,k,s,A,W = get_us_airport(); 
    s=np.array( s)
    k=np.array( g.degree() )
    A = np.array(A.data)
    n = s.shape[0]
    sol=solve_UDCGM(s,k=k,z0=None,jac=None)
    assert sol.success
    z = sol.x[0]
    P = pij_UDCGM(s,z)
    wij_sample = sample_wij_UDCGM(P,s,nsamp=300,correction=False)
    aij_sample = 1.*(wij_sample>0)
    aij_avg = aij_avg_UDCGM(wij_sample)
    k_avg = np.sum(aij_avg,axis=0)	
    knn_avg = knn_avg_UBCM(P,k_avg)
    knn_sigma = knn_sigma_sample(aij_sample,directed=False)
	"""
    plt.scatter(k,g.knn()[0], c='r',s=10 )
    plt.scatter(k_avg, knn_avg, c='b',s=2)
    plt.scatter(k_avg, knn_avg+knn_sigma, c='g',s=2)
    plt.scatter(k_avg, knn_avg-knn_sigma, c='g',s=2)
    plt.show()
	"""
def test_snn_avg_UDCGM(P,s):
	"""test samples """
    g,k,s,A,W = get_us_airport(); 
    s=np.array( s)
    k=np.array( g.degree() )
    A = np.array(A.data)
    n = s.shape[0]
    sol=solve_UDCGM(s,k=k,z0=None,jac=None)
    assert sol.success
    z = sol.x[0]
    P = pij_UDCGM(s,z)
    wij_sample = sample_wij_UDCGM(P,s,nsamp=300)
    aij_sample = 1.*(wij_sample>0)
    wij_avg= wij_avg_UDCGM(wij_sample)
    aij_avg = aij_avg_UDCGM(wij_sample)
    #aij_avg = aij_avg_UDCGM(wij_sample)
    k_avg = np.sum(aij_avg,axis=0)	
    s_avg = np.sum(wij_avg,axis=0)
    snn_avg = snn(P,k_avg,s_avg)
    snn_sigma = snn_sigma_sample(wij_sample,directed=False,correction=False)
	"""
    plt.scatter(s, snn(A,k,s) , c='r',s=10 )
    plt.scatter(s_avg, snn_avg, c='b',s=2)
    plt.scatter(s_avg, snn_avg+snn_sigma, c='g',s=2)
    plt.scatter(s_avg, snn_avg-snn_sigma, c='g',s=2)
    plt.show()
	"""	
def test_wknn_avg_UDCGM(P,s):
	"""test samples """
    g,k,s,A,W = get_us_airport(); 
    s=np.array( s)
    k=np.array( g.degree() )
    A = np.array(A.data)
    n = s.shape[0]
    sol=solve_UDCGM(s,k=k,z0=None,jac=None)
    assert sol.success
    z = sol.x[0]
    P = pij_UDCGM(s,z)
    wij_sample = sample_wij_UDCGM(P,s,nsamp=300,correction=False)
    aij_sample = 1.*(wij_sample>0)
    wij_avg= wij_avg_UDCGM(wij_sample)
    aij_avg = aij_avg_UDCGM(wij_sample)
    s_avg = np.sum(wij_avg,axis=0)	
    #
    wknn_ = wknn(np.array(W.data),s)
    wknn_avg =wknn(wij_avg,s_avg)
    wknn_sigma = wknn_sigma_sample(wij_sample,directed=False)
    """
    plt.scatter(s, wknn_ , c='r',s=10 )
    plt.scatter(s_avg, wknn_avg, c='b',s=2)
    plt.scatter(s_avg, wknn_avg+wknn_sigma, c='g',s=2)
    plt.scatter(s_avg, wknn_avg-wknn_sigma, c='g',s=2)
    plt.show()
	"""	
	
def test_knn_avg_UDCGM_comtrade():
	"""  """
    path = '{}/yrp-2000.csv'.format(PATH_COMTRADE)
    g,k,s,A,W = get_comtrade_igraph(path,directed=False)
    assert not g.is_directed()
    factor=1E-9
    #nmax = 40; 
    nmax = 226; 
    s=factor * np.array( s)#[0:nmax]
    #s=np.log(factor* np.array( s))#[0:nmax]
    k=np.array( g.degree() )#[0:nmax]
    A = np.array(A.data)
    n = s.shape[0]
    sol=solve_UDCGM(s,k=k,z0=None)#,jac=ficm_cy._jac_UDCGM)
    assert sol.success
    z = sol.x[0]
    P = pij_UDCGM(s,z)
    wij_sample = sample_wij_UDCGM(P,s,nsamp=300,correction=False)
    aij_sample = 1.*(wij_sample>0)
    aij_avg = aij_avg_UDCGM(wij_sample)
    k_avg = np.sum(aij_avg,axis=0)	
    knn_avg = knn_avg_UBCM(P,k_avg)
    knn_sigma = knn_sigma_sample(aij_sample,directed=False)
	"""
    plt.scatter(k,g.knn()[0], c='r',s=10 )
    plt.scatter(k_avg, knn_avg, c='b',s=2)
    plt.scatter(k_avg, knn_avg+knn_sigma, c='g',s=2)
    plt.scatter(k_avg, knn_avg-knn_sigma, c='g',s=2)
    plt.show()
	"""
    assert True	

def test_snn_avg_UDCGM_comtrade(P,s):
	"""test samples """
    path = '{}/yrp-2000.csv'.format(PATH_COMTRADE)
    g,k,s,A,W = get_comtrade_igraph(path,directed=False)
    assert not g.is_directed()
    factor=1E-9    
    nmax = 226; 
    s=factor * np.array( s)#[0:nmax]
    #s=np.log(factor* np.array( s))#[0:nmax]
    k=np.array( g.degree() )#[0:nmax]
    A = np.array(A.data)
    n = s.shape[0]
    sol=solve_UDCGM(s,k=k,z0=None,jac=None)
    assert sol.success
    z = sol.x[0]
    P = pij_UDCGM(s,z)
    wij_sample = sample_wij_UDCGM(P,s,nsamp=300,correction=False,correction=False)
    aij_sample = 1.*(wij_sample>0)
    wij_avg= wij_avg_UDCGM(wij_sample)
    aij_avg = aij_avg_UDCGM(wij_sample)
    #aij_avg = aij_avg_UDCGM(wij_sample)
    k_avg = np.sum(aij_avg,axis=0)	
    s_avg = np.sum(wij_avg,axis=0)
    snn_avg = snn(P,k_avg,s_avg)
    snn_sigma = snn_sigma_sample(wij_sample,directed=False)
	"""
	plt.semilogy(s, snn(A,k,s) , 'ro',markersize=10 ) 
	plt.semilogy(s_avg, snn_avg , 'b.',markersize=10 ) 
	plt.semilogy(s_avg, snn_avg+snn_sigma, 'g.',markersize=2 ) 
	plt.semilogy(s_avg,snn_avg-snn_sigma , 'g.',markersize=2 ) 
	plt.show()
	
    plt.scatter(s, snn(A,k,s) , c='r',s=10 )
    plt.scatter(s_avg, snn_avg, c='b',s=2)
    plt.scatter(s_avg, snn_avg+snn_sigma, c='g',s=2)
    plt.scatter(s_avg, snn_avg-snn_sigma, c='g',s=2)
    plt.show()
	"""		
	
def test_histbydij_indexing():
    """
    """
    A = np.zeros(3*3*2).reshape((3,3,2))
    A[:,:,0]=np.array([[1,2,3],[4,5,6],[7,8,9]])
    A[:,:,1]=np.array([[0.1,0.2,0.3],[0.4,0.5,0.6],[.7,.8,.9]])
    idx     = np.array([[True,False,False],[False,False,False],[False,False,False]])
    A[idx,:]==np.array([[1. , 0.1]]) 
    assert np.all(np.isclose(A[idx,:],np.array([[1. , 0.1]])))
def test_histbydij():
    """
    """
    pass    
# -------------
# UECM - Undirected Extended Configuration Model

def test_like_UECM_airport():
    g,k,s,A,W = get_us_airport(); 
    s=np.array( s)
    k=np.array( g.degree() )
    n = s.shape[0]
    l= cm_cy.like_UECM( 0.1*np.ones(2*n), k,s,n ) 
    assert True

def test_like_UECM_comtrade():
    path = '{}/yrp-2000.csv'.format(PATH_COMTRADE)
    g,k,s,A,W = get_comtrade_igraph(path,directed=False)
    factor=1E-11
    #nmax = 40; 
    nmax = 226; 
    s=factor * np.array( s)[0:nmax]
    k=np.array( g.degree() )[0:nmax]
    n = s.shape[0]
    l= cm_cy.like_UECM( 0.1*np.ones(2*n), k,s,n ) 
    assert True


def test_solve_ml_UECM_airport():
    g,k,s,A,W = get_us_airport(); 
    s=np.array( s)
    k=np.array( g.degree() )
    sol = solve_ml_UECM(k,s)
    assert sol.success
    """P = pij_UWCM(sol.x)
    # sample average
    n = P.shape[0]
    W=np.zeros((n,n,nsamp),dtype=np.long) 
    cm_cy.samples_UWCM( P, W) 
    s_avg, W_flat_avg= sample_avg_UWCM(W)
    #plt.scatter(s,s_avg); plt.show()
    r = pearsonr(s,s_avg)
    assert r[0]>0.9"""

def test_solve_ml_UECM_comtrade():
    path = '{}/yrp-2000.csv'.format(PATH_COMTRADE)
    g,k,s,A = get_comtrade_igraph(path,directed=False)
    factor=1E-11
    nmax = 40; maxiter = 10000
    #nmax = 226; maxiter = 100000
    s=factor * np.array( s)[0:nmax]
    k=np.array( g.degree() )[0:nmax]
    sol = solve_ml_UECM(k,s,method=None)
    assert sol.success

def test_solve_ml_UECM_jac_comtrade():
	""" ML+jac"""
    path = '{}/yrp-2000.csv'.format(PATH_COMTRADE)
    g,k,s,A = get_comtrade_igraph(path,directed=False)
    factor=1E-11
    nmax = 40; maxiter = 10000
    #nmax = 226; maxiter = 100000
    s=factor * np.array( s)[0:nmax]
    k=np.array( g.degree() )[0:nmax]
    sol = solve_ml_UECM(k,s,method='Newton-CG',jac=cm_cy.jac_like_UECM)
    assert sol.success

def test_solve_eq_UECM_airport():
	""" problem is smaller than the original
	options: https://docs.scipy.org/doc/scipy/reference/optimize.root-lm.html#optimize-root-lm
	"""
    g,k,s,A,W = get_us_airport(); 
    #nmax = 40; maxiter=20000
    nmax = 332; maxiter = 200000
    factor = 0.1
    s=factor * np.array( s)[0:nmax]
    k=np.array( g.degree() )[0:nmax]
    sol = solve_eq_UECM(k,s,method='lm', options={'maxiter':maxiter})
    #sol = solve_eq_UECM(k,s,method='lm', jac =jac_UECM)
    #np.save('data/airport_UECM.npy',sol.x)
    assert sol.success
    err = layereq_UECM(sol.x,k,s)
    
def test_solve_iter_UECM_two_step():
    """ """
    g,k,s,A,W = get_us_airport();
    pij_ts,wij_avg = solve_UECM_two_step(k,s,solve_wij='iter',loops=1000,precision=10e-2)
    plt.scatter(W.flatten(),wij_avg ); 
    plt.show()
    plt.scatter(np.sum(W,axis=0),s )
    plt.show()
def test_solve_root_UECM_two_step():
    """ """
    g,k,s,A,W = get_us_airport();
    pij_ts,wij_avg = solve_UECM_two_step(k,s,solve_wij='iter',loops=1000,precision=10e-2)
    plt.scatter(W.flatten(),wij_avg ); 
    plt.show()
    plt.scatter(np.sum(W,axis=0),s )
    plt.show()

def test_solve_eq_UECM_comtrade():
	""" """
    path = '{}/yrp-2000.csv'.format(PATH_COMTRADE)
    g,k,s,A = get_comtrade_igraph(path,directed=False)
    factor=1E-11
    nmax = 40; maxiter = 10000
    #nmax = 226; maxiter = 100000
    s=factor * np.array( s)[0:nmax]
    k=np.array( g.degree() )[0:nmax]
    sol = solve_eq_UECM(k,s,method='lm', options={'maxiter':maxiter})
    #np.save('data/comtrade_yrp_UECM.npy',sol.x)
    #np.savez('data/comtrade_yrp_UECM.npz',h=sol.x,s=s,k=k)
    assert sol.success
    err = layereq_UECM(sol.x,k,s)
    err/np.hstack((k,s))

def test_pij_UECM():
	""" """
	#path = 'data/airport_UECM.npy'
    path = 'data/comtrade_yrp_UECM.npz'
    npzfile=np.load(path)
    P = pij_UECM(npzfile['h'])
    k_avg = P.sum(axis=0)
    k=npzfile['k']
    plt.scatter(k,k_avg); plt.show()
    r = pearsonr(k,k_avg)
    assert r[0]>0.9
    
def test_wij_avg_UECM():
	""" """
    #h=np.load('data/airport_UECM.npy')
    path = 'data/comtrade_yrp_UECM.npz'
    npzfile=np.load(path)
    W_avg = wij_avg_UECM(npzfile['h'])    
    s_avg = W_avg.sum(axis=0)
    s=npzfile['s']
    plt.scatter(s,s_avg); plt.show()
# -------------
# DBCM - Directed Binary Configuration Model

def test_indegree():
    """why do we get different results for g.indegree and np.sum(A,axis=1) """
    path = '{}/yrp-2000.csv'.format(PATH_COMTRADE)
    g,k,s,A = get_comtrade_igraph(path)
    kin=np.array( g.indegree() ,dtype=np.int32 )
    kout=np.array( g.outdegree() ,dtype=np.int32 )    
    k_in  = np.sum(A,axis=0)
    k_out  = np.sum(A,axis=1)

def test_solve_eq_DBCM():
	""" """
	# get data
    path = '{}/yrp-2000.csv'.format(PATH_COMTRADE)
    g,k,s,A = get_comtrade_igraph(path)
    kin=np.array( g.indegree() ,dtype=np.int32 )
    kout=np.array( g.outdegree() ,dtype=np.int32 )
    nin = kin.shape[0]
    nout = kout.shape[0]
    sol = solve_eq_DBCM(kin,kout)
    assert sol.success
    P = pij_DBCM(sol.x,nout,nin)
    #assert np.any(P>1)
    #assert np.any(P<0)
    rin = pearsonr(np.sum(P,axis=0),kin)
    rout = pearsonr(np.sum(P,axis=1),kout)
    assert rin[0]>0.9
    assert rout[0]>0.9
	#plt.subplot(1,2,1); plt.scatter(np.sum(P,axis=0),kin); 
	#plt.subplot(1,2,2); plt.scatter(np.sum(P,axis=1),kout); 
	#plt.show() 

def test_layereq_DBCM():
	# get data
    path = '/home/aurelien/local/data/comtrade/yrp-2000.csv'
    g,k,s,A = get_comtrade_igraph(path)
    kin=np.array( g.indegree() ,dtype=np.int32 )
    kout=np.array( g.outdegree() ,dtype=np.int32 )
    nin = kin.shape[0]
    nout = kout.shape[0]
	#      
	x0 = np.empty((nin+nout)) 
    x0[0:nout] = kout/np.average(kout) 
    x0[nout:(nout+nin)] = kin/np.average(kin) 
	y=layereq_DBCM(x, kout, kin, nout, nin)


# -------------
# UWCM - Undirected Weighted Configuration Model


def test_solve_eq_UWCM():
    s = s_test()
    sol = solve_eq_UWCM(s)
    assert True

def test_solve_eq_UWCM_converge():
    s = s_test()
    sol = solve_eq_UWCM(s)
    assert sol.success

def test_solve_eq_UWCM_diagonal():
    """ """
    s = s_test()
    sol = solve_eq_UWCM(s)
    P = pij_UWCM(sol.x)		
    assert np.all(P.diagonal()==0)

def test_solve_eq_UWCM_domain():
	""" x_i x_j must be in [0,1]  
		cf [Squartini et al.14] around eq.(33)"""	
    s = s_test()
    sol = solve_eq_UWCM(s)
    P = pij_UWCM(sol.x)
    assert np.all( P<1) and np.all( P>=0)

def test_solve_eq_UWCM_avg():
	"""
	check that	<s_i> = s_i
	NB: 
	<s_i> = \sum_{j \neq i} \frac{x_i x_j}{1- x_i x_j}
	"""
    s = s_test()
    sol = solve_eq_UWCM(s)		
    s_avg = np.sum( eq_UWCM(sol.x), axis=0)
    assert np.all(np.isclose(s,s_avg))

def test_sample_UWCM():
    s = s_test()
    sol = solve_eq_UWCM(s)
    P = pij_UWCM(sol.x)
    W = sample_UWCM(P)
    assert True

def test_samples_UWCM():
	""" """
	n= 50
	nsamp = 100
	P = np.random.rand(n*n).reshape(n,n) 
	W=np.zeros((n,n,nsamp),dtype=np.long)
	cm_cy.samples_UWCM( P, W)
	# this one is much slower: cm_cy.samples_UWCM2( P, W)
    
"""    
def test_sample_avg_UWCM_airport():
    #s = 10.*k_test('powerlaw')
    #s = s_test()
    k,s = get_us_airport(); s=np.array( s)
    nsamp = 10
    sol = solve_eq_UWCM(s)
    assert sol.success
    P = pij_UWCM(sol.x)
    # sample average
    s_avg, W_flat_avg= sample_avg_UWCM(P,nsamp)
    #plt.scatter(s,s_avg); plt.show()
    r = pearsonr(s,s_avg)
    assert r[0]>0.9
"""    
def test_sample_avg_UWCM_airport():
    #s = 10.*k_test('powerlaw')
    #s = s_test()
    g,k,s,A,W = get_us_airport(); s=np.array( s)
    nsamp = 10
    sol = solve_eq_UWCM(s)
    assert sol.success
    P = pij_UWCM(sol.x)
    # sample average
    n = P.shape[0]
    W=np.zeros((n,n,nsamp),dtype=np.long) 
    cm_cy.samples_UWCM( P, W) 
    s_avg, W_flat_avg= sample_avg_UWCM(W)
    #plt.scatter(s,s_avg); plt.show()
    r = pearsonr(s,s_avg)
    assert r[0]>0.9

    
# -------------
# UBCM - Undirected Binary Configuration Model

def test_like_UBCM():
    k = k_test()
    n = k.shape[0]
    cm_cy.like_UBCM( np.ones(n), k,n ) 
    assert True

def test_jac_like_UBCM():
    k = k_test()
    n = k.shape[0]
    jac_ = cm_cy.jac_like_UBCM( 0.5*np.ones(n), k,n ) 
    assert True

def test_solve_eq_UBCM():
    k = k_test()
    sol = solve_eq_UBCM(k)
    assert sol.success

def test_solve_eq_UBCM_dense():
	""" dense network """
    path = '/home/aurelien/local/data/comtrade/yrp-2000.csv'
    g,k,s,A = get_comtrade_igraph(path)
    g_undir = g.copy()
    g_undir.to_undirected()
    k_undir = np.array(g_undir.degree(),np.int)
    assert k_undir.max()<g_undir.vcount()
    sol = solve_eq_UBCM(k_undir)
	assert sol.success
	#
	P = pij_UBCM(sol.x)
	assert np.any(P>1)
	assert np.any(P<0)

def test_solve_eq_UBCM_converge():
    k = k_test()
    sol = solve_eq_UBCM(k)
    assert sol.success


def test_solve_ml_UBCM():
    #solver=ML
    k = k_test()
    sol = solve_ml_UBCM(k)
    assert True

def test_pij_ml_UBCM():
    #solver=ML
    k = k_test()
    sol = solve_ml_UBCM(k)	
    P = pij_UBCM(sol.x)
    assert True    

def test_pij_ml_jac_UBCM():
    #solver=ML
    k = k_test()
    sol = solve_ml_UBCM(k)	
    P = pij_UBCM(sol.x)
    assert True 

def test_pij_eq_UBCM():
	#solver=eq
    k = k_test()
    sol = solve_eq_UBCM(k)	
    P = pij_UBCM(sol.x)
    assert True        

def test_solve_eq_UBCM_diagonal():
    """ """
    k = k_test()
    sol = solve_eq_UBCM(k)	
    P = pij_UBCM(sol.x)	
    assert np.all(P.diagonal()==0)

def test_solve_ml_UBCM():
	""" """
    g,k_air,s,A,W = get_us_airport_igraph(path='{}/data/USAir97.net'.format(ROOT_DIR))
    k_com=np.array([21, 39, 39, 21, 21, 39, 21, 21, 39, 21, 37, 21, 19, 21, 21,
		21, 21, 37, 21, 19, 39, 39 ,39, 21, 39, 21, 21, 39 ,39, 39, 39, 39, 39,
		39, 39, 39, 39 ,39 ,21, 21]) 
	for k in [k_air,k_com]:	
		k=np.array( k ,dtype=np.int32 )
		x0 = k / k.mean()
		sol = solve_ml_UBCM(k,x0=x0)
		#assert sol.success
		P = pij_UBCM(sol.x)
		r = pearsonr(np.sum(P,axis=0),k)
		assert r[0]>0.9
		#plt.scatter(np.sum(P,axis=0),k); plt.show() 
	

def test_sample_UBCM():
    k = k_test('powerlaw')
    nsamp = 1000

    for f_label,f in [('ml',solve_ml_UBCM),('eq',solve_eq_UBCM)]:
        sol = f(k)	
        P = pij_UBCM(sol.x)
        # average degree  
        print("###################\n# SOLVER={}".format(f_label))
        print(np.sum(P,axis=0)-k)
        # mcmc        
        k_avg = np.zeros(k.shape[0])    
        for i in range(nsamp):
            A = sample_UBCM(P)
			#print(np.sum(A,axis=0))
            k_avg += 	np.sum(A,axis=0)
        k_avg /= nsamp 
        print(k_avg-k)    
    assert True

def test_samples_UBCM():
	""" """
	n = 500
	nsamp = 100
	P = np.random.rand(n*n).reshape((n,n)) 
	rnd = np.random.rand(n*n*nsamp).reshape((n,n,nsamp)) 
	A=samples_UBCM(P, rnd)
	
	
def test_knn_avg_UBCM():
	""" """
    g,k,s,A,W = get_us_airport_igraph(path='../data/USAir97.net')
    k=np.array( k ,dtype=np.int32 )
    sol = solve_eq_UBCM(k)
    assert sol.success
    P = pij_UBCM(sol.x)
    knn = knn_avg_UBCM(P,k)
	# plot: cf examples.ipynb

def test_knn_avg_UBCM_compare():
	""" compare two algos that compute knn"""
    g,k,s,A,W = get_us_airport_igraph(path='../data/USAir97.net')
    k=np.array( k)
    mat = np.array(g.get_adjacency().data)
    knn_mat = knn_avg_UBCM(mat,k)
    ann_per_vertex,ann_per_deg = g.knn()
    assert np.all( knn_mat == ann_per_vertex)
    #n=knn_mat.shape[0]; plt.scatter(knn_mat ,ann_per_vertex); plt.show()
    #plt.plot(range(n), knn_mat, range(n),ann_per_vertex); plt.show()
	

def test_knn_sigma_sample():
	""" """
    g,k,s,A,W = get_us_airport_igraph(path='../data/USAir97.net')
    k=np.array( k ,dtype=np.int32 )
    sol = solve_eq_UBCM(k)
    assert sol.success
    P = pij_UBCM(sol.x)
    # sample
    nsamp=100
    n = P.shape[0]
    rnd = np.random.rand(n*n*nsamp).reshape((n,n,nsamp))
    A=samples_UBCM(P, rnd)
    knn_sigma = knn_sigma_sample(A)
    # plot: cf examples.ipynb	
	
def test_snn():
	""" """
    g,k,s,A,W = get_us_airport_igraph(path='data/USAir97.net')
    k=np.array( k )
    snn_ = snn(np.array(A.data),s,k)
	# plot: cf examples.ipynb
	
def test_snn_avg():
	""" the same algorithm (snn) is used, but replacing with average quantities:
		Pij instead of Aij
		<ki> instead of k
		(<si> =s by definition of the WBCM )
	"""
    g,k,s,A,W = get_us_airport_igraph(path='data/USAir97.net')
    sol = solve_eq_UWCM(s) 
    assert sol.success
    P = pij_UWCM(sol.x)
    k_avg_ = k_avg_UBCM(P)  
    snn_avg = snn(P,s,k_avg_)    
	# plot: cf examples.ipynb

def test_snn_sigma_sample(W,directed=False)
	""" """
	n= 50
	nsamp = 100
	P = np.random.rand(n*n).reshape(n,n) 
	W=np.zeros((n,n,nsamp),dtype=np.long)
	cm_cy.samples_UWCM( P, W)
	snn_sigma = snn_sigma_sample(W,directed=False)

def test_wknn(W,s):
	""" """
	g,k,s,A,W = get_us_airport_igraph(path='data/USAir97.net')
	W = g.get_adjacency(type=GET_ADJACENCY_BOTH, attribute='weight', default=0, eids=False)
	wknn_ = wknn(np.array(W.data),s)
	# plot: cf examples.ipynb
	
def test_wknn_avg(W,s):
	""" """
	raise NotImplementedError
	#wknn(W_avg,s)

"""
TODO: compare knn_avg to graph_based algorithms
"""
